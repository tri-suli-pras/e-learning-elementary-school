class TableController 
{
    constructor( compId )
    {
        this.compId = compId;

        TableController.render(this.compId);
    }

    static render(compId) 
    {
        return $(compId).DataTable({
            "language": {
                "lengthMenu": "Tampil _MENU_",
                "zeroRecords": "-- Kosong --",
                "info": "Menampilkan _START_ sampai _END_ dari total _TOTAL_ data",
                "infoEmpty": "Data tidak ditemukan",
                "infoFiltered": "(filter dari _MAX_ total data)",
                "search": "Cari"
            }  
        });
    }
}