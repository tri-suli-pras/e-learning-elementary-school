// handle overite file text
$(function() {
	let error = "is-invalid",
		urlname = window.location.href,
		inputTitle = $(`input[name = title]`);
		inputTime = $(`input[name = time]`); 
		inputFile = $(`input[name = file_path]`); 

	function changeInputFileText(name) {

		let label = $(`label[for = ${name}]`);

		$(`input[name = ${name}]`).change(function() {
		
			let filename = $(this).val();

			filename = filename.split('\\')[2];
			label.text(filename);
		});
	}

	changeInputFileText('file_path');

	if (string_exist(urlname, "edit")) {
		
		if (!inputTitle.hasClass(error)) {
		
			inputTitle.keyup(function() {
	                
	            let message = $(this).next();
	            let countLetter = $(this).val().length;
	            
	            if (countLetter < 1) {
	                
	                $(this).addClass(error);
	                message.text(errors.title.required);
	                message.show();
	            }
	            else if (countLetter > 75) {
					
					$(this).addClass(error);
	                message.text(errors.title.max);
	                message.show();
	            }
	            else {

	                $(this).removeClass("is-invalid");
	                message.hide();
	            }
	        });
    	}

    	if (!inputTime.hasClass(error)) {
			
			inputTime.change(function() {
				
				let val = $(this).val();

				$(this).next().text(errors.time.required);

                if (val == '') {
                    
                    $(this).addClass(error);
                    $(this).next().show();
                }
                else {

                    $(this).removeClass(error);
                    $(this).next().hide();
                }
			});
    	}

    	if (!inputFile.hasClass(error)) {
			
			inputFile.change(function() {
		
				let filename = $(this).val();

				filename = filename.split('\\')[2];
				
				if (!string_exist(filename, ".xlsx") || !string_exist(filename, ".xls")) {
					
					inputFile.next().text(errors.file_path.mimes);
					inputFile.addClass(error);
					inputFile.next().show(errors.file_path.mimes);
					$("#submit").attr({
						"disabled": true,
						"data-toggle": "tooltip",
						"title": "Periksa kembali input data anda"
					});
				}
				else {
					inputFile.next().text('');
					inputFile.removeClass(error);
					inputFile.next().hide();
					$("#submit").attr({
						"disabled": false,
						"data-toggle": "",
						"title": ""
					});
				}
			});
    	}
	}
});