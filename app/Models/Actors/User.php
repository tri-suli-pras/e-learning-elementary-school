<?php

namespace App\Models\Actors;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Traits\HasPermissionsTrait;
use App\Models\Management\UserTeacher;
use App\Models\Data\{
    Lesson,
    News,
    Quiz,
    Task,
    Material
};

class User extends Authenticatable
{
    use Notifiable, HasPermissionsTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'pegawai_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function teach()
    {
        return $this->belongsToMany(Lesson::class, "el_teacher_lessons", "user_id", "lesson_id");
    }

    public function employees()
    {
        return $this->hasOne(UserTeacher::class, "user_id");
    }

    public function news()
    {
        return $this->hasMany(News::class, "user_id", 'id');
    }

    public function quizs()
    {
        return $this->hasMany(Quiz::class, "user_id", "id");
    }

    public function tasks()
    {
        return $this->hasMany(Task::class, "user_id", "id");
    }
}
