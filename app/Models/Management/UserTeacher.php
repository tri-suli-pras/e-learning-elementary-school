<?php

namespace App\Models\Management;

use Illuminate\Database\Eloquent\Model;
use App\Models\Actors\User;

class UserTeacher extends Model
{
    protected $table = "user_pegawai";

    protected $fillable = [
		'user_id',
		'pegawai_id'
    ];

    public function account()
    {
    	return $this->belongsTo(User::class, "user_id");
    }

    public function teachers()
    {
    	return $this->belongsTo(Employee::class, "pegawai_id");
    }
}
