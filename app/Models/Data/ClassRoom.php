<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;
use App\Models\Management\Employee;

class ClassRoom extends Model
{
    protected $table = "kelas";

    public function homeRoomTeacher()
    {
    	return $this->belongsTo(Employee::class, "pegawai_id", "id");
    }

    public function materials()
    {
    	return $this->hasMany(Material::class, "class_id", 'id');
    }

    public function quizs()
    {
    	return $this->hasMany(Quiz::class, "class_id", "id");
    }

    public function classTasks()
    {
        return $this->haveMany(Test::class, "class_id", "id");
    }

    public function news()
    {
        return $this->hasMany(News::class, 'news_id');
    }
}
