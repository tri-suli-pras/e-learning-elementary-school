<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
    /**
     * @property [#] $table <define table>
     */
    protected $table = "kurikulum";

 	public function lessons()
 	{
 		return $this->hasMany(Lesson::class, 'kurikulum_id');
 	}   
}
