<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;
use App\Models\Actors\User;

class Lesson extends Model
{
    protected $table = "matapelajaran";
    
    public function taught()
    {
        return $this->belongsToMany(User::class, "el_teacher_lessons", "lesson_id", "user_id");
    }

    public function materials()
    {
		return $this->hasMany(Material::class, "lesson_id", 'id');
    }

    public function quizs()
    {
    	return $this->hasMany(Quiz::class, "lesson_id", 'id');
    }

    public function tasks()
    {
    	return $this->hasMany(Task::class, "lesson_id", "id");
    }

    public function curriculums()
    {
        return $this->belongsTo(Curriculum::class, 'kurikulum_id');
    }
}
