<?php 

namespace App\Models\Data\Traits;

trait DocumentDirectory
{
	/**
     * Create directory for storing
     * User management asset.
     *
     * @param String $path
     * @param Array $properties
     * @param String $filename : DEFAULT null
     * @return String 
     */
	public static function saveAsset(string $path, array $properties, string $filename = null)
	{
		$cls = $properties['class_room'].'-'.$properties['pararel'];
		$uid = $properties['user_id'];
		$lid = $properties['lesson_id'];

		$dir = "storage/management/$path/uid_$uid/cls_$cls/lid_$lid";

		if (! is_null($filename))
		{
			return $dir .= '/'.$filename;
		}

		return $dir;
	}
}