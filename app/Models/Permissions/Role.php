<?php

namespace App\Models\Permissions;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guard = [];

    protected $table = "roles";

    public function permissions()
    {
    	return $this->belongsToMany(Permission::class, 'roles_permissions');
    }
}
