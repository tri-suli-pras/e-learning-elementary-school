<?php declare(strict_types=1);

namespace App\Libraries\Contracts\AutomatedGrading\Corrector;

interface EssayCorrection
{
	/**
	 * Menyiapkan jawaban essay yang akan dikoreksi.
	 * 
	 * @param  array $values
	 * @return void
	 */
	public function setEssayAnswers(...$values) : Void;
}