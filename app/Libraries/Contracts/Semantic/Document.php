<?php declare(strict_types=1);

namespace App\Libraries\Contracts\Semantic;

use Illuminate\Contracts\Support\Arrayable;

interface Document extends Arrayable
{
	/**
	 * Mendapatkan isi dari document.
	 * 
	 * @return string|null
	 */
	public function getBody();

	/**
	 * Mendapatkan judul dokumen.
	 * 
	 * @return string
	 */
	public function getTitle() : String;

	/**
	 * Menyiapkan isi dokumen.
	 * 
	 * @param  string $text
	 * @return void
	 */
	public function setBody(string $text) : Void;

	/**
	 * Menyiapkan judul dokumen.
	 * 
	 * @param  string $text
	 * @return void
	 */
	public function setTitle(string $text) : Void;
}