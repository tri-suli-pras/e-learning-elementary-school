<?php declare(strict_types=1);

namespace App\Libraries\AutomatedGrading\Corrector;

use App\Libraries\AutomatedGrading\QuestionCorrector;

class MultipleChoiceAnswerCorrector extends QuestionCorrector
{
	/**
	 * Pilihan jawaban.
	 * 
	 * @var array
	 */
	protected $answerChoice;

	/**
	 * Prosedur perhitungan nilai.
	 * 
	 * @return float
	 */
	protected function calculatedResult() : Float
	{
		// Jumlah jawaban benar.
		$correct = $this->getCorrectAnswers();

		// bobot nilai
		$weight = $this->getPercentage();

		// (jumlah jawaban benar / jumlah soal) * bobot nilai.
		$result = ($correct / $this->count()) * $weight;

		return $result;
	}

	/**
	 * Menjalankan prosedur koreksi soal.
	 *
	 * @return void
	 */
	public function correction() : Void
	{
		$references = $this->getAnswersKey();
		$terms = $this->getAnswers();

		foreach ($terms as $key => $term) {
		 	if (strtolower($term) === strtolower($references[$key])) {
		 		$this->correctAnswer[] = ++$key;
		 	}
		 	else {
		 		$this->wrongAnswer[] = ++$key;
		 	}
		}

		$this->score = $this->calculatedResult();
	}

	/**
	 * Mendapatkan pilihan jawaban soal.
	 * 
	 * @return array
	 */
	public function getAnswersChoices() : Array 
	{
		return $this->answerChoice;
	}
}