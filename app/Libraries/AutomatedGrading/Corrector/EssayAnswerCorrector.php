<?php declare(strict_types=1);

namespace App\Libraries\AutomatedGrading\Corrector;

use App\Libraries\AutomatedGrading\QuestionCorrector;
use App\Libraries\Semantic\Document;
use App\Libraries\Semantic\LatentSemanticAnalysis;

class EssayAnswerCorrector extends QuestionCorrector
{
	public $answersScore;

	/**
	 * Menjalankan prosedur koreksi soal.
	 *
	 * @return void
	 */
	public function correction() : Void
	{
		$point = $this->calculatedResult();

		$this->score = (($point / count($this->getQuestions())) / 100) * $this->getPercentage();
	}

	/**
	 * Prosedur perhitungan nilai.
	 * 
	 * @return float
	 */
	protected function calculatedResult() : Float
	{
		foreach ($this->getAnswersKey() as $key => $answersKey) {
			$lsa = new LatentSemanticAnalysis(
				new Document('training', $answersKey),
	    		new Document('testing', $this->getAnswers()[$key])
			);
			$lsa->analyze();
			$result = $lsa->getResult();

			if ($result > 75) {
				$this->correctAnswer[] = ++$key;
			}
			else {
				$this->wrongAnswer[] = ++$key;
			}

			$this->answersScore[] = $result;
		}

		return collect($this->answersScore)->sum();
	}
}