<?php declare(strict_types=1);

namespace App\Libraries\AutomatedGrading;

use App\Libraries\AutomatedGrading\Corrector\EssayAnswerCorrector;
use App\Libraries\AutomatedGrading\Corrector\MultipleChoiceAnswerCorrector;
use App\Libraries\Contracts\AutomatedGrading\Corrector\EssayCorrection;
use App\Libraries\Contracts\AutomatedGrading\Corrector\MultipleChoiceCorrection;

final class AutomatedScoring implements EssayCorrection, MultipleChoiceCorrection
{
	/**
	 * Korektor soal pilihan ganda.
	 * 
	 * @var MultipleChoiceAnswerCorrector
	 */
	private $multipleChoice;

	/**
	 * Korektor soal essay.
	 * 
	 * @var EssayAnswersCorrector
	 */
	private $essay;

	/**
	 * Nilai akhir
	 * 
	 * @var float
	 */
	protected $score = 0.0;

	/**
	 * Menyiapkan tipe soal untuk koreksi soal.
	 * 
	 * @param  string $questionType        
	 * @param  int    $multipleChoiceWeight
	 * @param  int    $essayWeight   
	 * @return void      
	 */
	public function __construct(string $questionType, int $multipleChoiceWeight, int $essayWeight)
	{
		// Load soal dan kunci jawaban
		$qna = $this->loadQuestion($questionType);

		$this->multipleChoice = new MultipleChoiceAnswerCorrector($qna['multiple_choice'], $multipleChoiceWeight);
		$this->essay = new EssayAnswerCorrector($qna['essay'], $essayWeight);
	}

	/**
	 * Mendapatkan nilai properti secara dinamis.
	 * 
	 * @param  string $prop
	 * @return any
	 */
	public function __get($prop)
	{
		return $this->{$prop};
	}

	/**
	 * Mendapatkan nilai akhir.
	 * 
	 * @return float
	 */
	public function getScore() : Float 
	{
		return $this->score;
	}

	/**
	 * Mengakses file soal.
	 * 
	 * @param  string $pathToFile
	 * @return array           
	 */
	private function loadQuestion(string $pathToFile) : Array 
	{
		return require base_path($pathToFile);
	}

	/**
	 * Menjalankan prosedur koreksi soal.
	 * 
	 * @return void
	 */
	public function proceed() : Void
	{
		// Menjalankan proses koreksi soale essay.
		$this->essay->correction();

		// Menjalankan proses koreksi soal pilihan ganda.
		$this->multipleChoice->correction();

		return;
	}

	/**
	 * Menyiapkan jawaban essay yang akan dikoreksi.
	 * 
	 * @param  array $values
	 * @return void
	 */
	public function setEssayAnswers(...$values) : Void
	{
		$this->essay->setAnswers($values);
	}

	/**
	 * Menyiapkan jawaban pilihan ganda yang akan dikoreksi.
	 * 
	 * @param  array $values
	 * @return void
	 */
	public function setMultipleChoiceAnswers(...$values) : Void
	{
		$this->multipleChoice->setAnswers($values);
	}
}