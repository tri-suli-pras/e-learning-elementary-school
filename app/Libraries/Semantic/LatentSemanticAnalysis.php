<?php declare(strict_types=1);

namespace App\Libraries\Semantic;

use App\Libraries\Contracts\Semantic\Document;
use App\Libraries\Semantic\Document as DocumentBuilder;
use App\Libraries\Semantic\DocumentMatrix;
use App\Libraries\Semantic\Math\SingularValueDecomposition;
use NlpTools\Similarity\CosineSimilarity;
use Phpml\Math\LinearAlgebra\EigenvalueDecomposition;
use Phpml\Math\Matrix;

class LatentSemanticAnalysis
{
	private $svd;

	private $cosine;

	private $matrixDoc;

	protected $result;

	public function __construct(Document $query, Document $testing)
	{
		$this->matrixDoc = new DocumentMatrix($query, $testing);
		$this->svd = new SingularValueDecomposition();
		$this->cosine = new CosineSimilarity();
	}

	public function analyze()
	{
		$cos = new CosineSimilarity();
		$termDocument = $this->matrixDoc->getData('testing');
		$refrenceDocument = $this->matrixDoc->getData('training');
		$result = $cos->similarity($refrenceDocument, $termDocument);
	    $this->result = $result;

		$this->result = round($result) * 100;

		return $this;
	}

	public function getResult()
	{
		return $this->result;
	}
}