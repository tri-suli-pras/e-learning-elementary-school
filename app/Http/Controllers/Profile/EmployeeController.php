<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\ORM_EmployeeRepository;   
use App\Http\Requests\UpdateEmployeeProfile;

class EmployeeController extends Controller
{
    /**
     * @property [#] employee 
     */
	protected $employee;

    /**
     * @param  \App\Repositories\Eloquent\ORM_EmployeeRepository $employee
     * @return void 
     */
	public function __construct(ORM_EmployeeRepository $employee)
	{
		$this->employee = $employee;
	}
    
    /**
     * @param Int $id <user ID>
     * @return View 
     */
    public function show(int $id)
    {
    	$employee = $this->employee->findWithAccount($id);
     
    	return view(dashboard("user_management.employee", "show"), compact('employee'));
    }

    public function edit($id)
    {
        $employee = $this->employee->find($id);
        $birthday = explode(',', $employee->ttl)[1];
        $place = explode(',', $employee->ttl)[0];
        $employee->ttl = $birthday;

        return view(dashboard("user_management.employee", "storeORupdate"), compact('employee', 'place'));
    }

    public function update($id, Request $request, UpdateEmployeeProfile $validator)
    {
        $validator->validated();
        
        $properties =  $request->except("_token", "_method");

        if ($request->hasFile('foto_pegawai')) {
            
            $properties['foto_pegawai'] = $request->foto_pegawai->getClientOriginalName();
            $this->employee->updateWithImageFile($id, $properties);
        }
        else 
            $this->employee->updateProfile($id, $properties);
        
        return to_route_with_param_and_flag(
            "management.employee.show", 
            $id, 
            "update",
            trans("confirmation.update", ['data' => "Profil"]) 
        );
    }
}
