<?php

namespace App\Http\Controllers\Profile;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\ORM_StudentRepository;
use App\Http\Requests\UpdateStudentProfile;

class StudentController extends Controller
{
    /**
     * @property [#] $student 
     */
	protected $student;
    
    /**
     * @param \App\Repositories\Eloquent\ORM_StudentRepository $student
     * @return void 
     */
    public function __construct(ORM_StudentRepository $student)
    {
		$this->student = $student;
    }
    
    /**
     * Student profile page
     * 
     * @param int $id <student ID>
     * @return View 
     */
    public function show($id)
    {
    	$student = $this->student->getAdministration($id);
        
        $months  = DB::table("bulan")->select("id", "bulan")->get();

        return view(dashboard("profile", "show"), compact('student', 'months'));
    }
    
    /**
     * Edit Student profile page
     * 
     * @param int $id <student ID>
     * @return View 
     */
    public function edit(int $id)
    {
        $student = $this->student->find($id);

        return view(dashboard("profile", "edit"), compact('student'));
    }
    
    /**
     * Update Student profile
     * 
     * @param int $id <student ID>
     * @param \Illuminate\Http\Request $request
     * @return View 
     */
    public function update(int $id, Request $request, UpdateStudentProfile $validator)
    {
        $validator->validated();
        
        $properties = $request->except('_token', '_method');

        if ($request->hasFile("foto_siswa")) {
            
            $nisn = $request->nisn;
            $properties['foto_siswa'] = $request->foto_siswa->getClientOriginalName();
           /* $properties['file_path'] = "public/student_{$id}/{$request->foto_siswa->getClientOriginalextension()}";
            $properties['file'] = $request->foto_siswa;
            dd($properties); */   
            $this->student->updateWithImageFile($id, $properties);
        }
        else
            $this->student->update($id, $properties);
    
        return to_route_with_param_and_flag(
            "student.profile", 
            $id, 
            "update", 
            trans('confirmation.update', ['data' => "Profile"]));
    }
}
