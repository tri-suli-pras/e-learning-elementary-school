<?php

namespace App\Http\Controllers\Questionnaire;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Cache;
use App\Services\Quiz\QuestionsControl;
use App\Services\OfficeDocuments\OfficeExcel;
use App\Repositories\Eloquent\ORM_QuizRepository as Quiz;

class StudentController extends Controller
{
	/**
	 * Repositori model.
	 * 
	 * @var Quiz
	 */
	protected $quiz;

	/**
	 * Membuat instance baru.
	 * 
	 * @param  Quiz $quiz
	 * @return Void 
	 */
	public function __construct(Quiz $quiz)
	{
		$this->quiz = $quiz;
	}

	/**
	 * Membuat dan menyimpan soal kuis untuk tiap siswa 
	 * yang mengikuti kuisioner lalu menampilkan soal kedalam 
	 * halaman web.
	 * 
	 * @param  Int              $quiz_id 
	 * @param  OfficeExcel      $excel   
	 * @param  QuestionsControl $question
	 * @return \Illuminate\View\View                 
	 */
    public function create(Int $quiz_id, OfficeExcel $excel, QuestionsControl $question)
    {
    	$quiz = $this->quiz->findWithLessons($quiz_id);
        $questions = $question->from($quiz->file_path, $excel);

        $nisn = student()->getNisn();
        $lecture = "_lecture_".$quiz->lessons->nama_matapelajaran."_quiz_id_$quiz->id";
        $key = $nisn . $lecture;
        $questions = $questions->randomize()->get();
        $questions = [
            'multiple' => $questions['multiple'],
            'essay' => $questions['essay'],
            'time' => Carbon::parse($quiz->starting_time)->addMinutes($quiz->count_down)->toDateTimeString()
        ];
        
        Cache::add($key, $questions, $quiz->count_down);

        $questions = Cache::get($key);

    	return view(dashboard("quiz", "show"), compact('quiz', 'questions'));
    }
}
