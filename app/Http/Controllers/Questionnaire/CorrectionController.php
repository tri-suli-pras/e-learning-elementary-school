<?php

namespace App\Http\Controllers\Questionnaire;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Services\Quiz\QuestionsControl;
use App\Http\Requests\StoreScoreStudents;
use App\Services\OfficeDocuments\OfficeExcel;
use App\Repositories\Eloquent\ORM_QuizRepository as Quiz;

class CorrectionController extends Controller
{
    /**
	 * Repositori model.
	 * 
	 * @var Quiz
	 */
	protected $quiz;

	/**
	 * Membuat instance baru.
	 * 
	 * @param  Quiz $quiz
	 * @return Void 
	 */
	public function __construct(Quiz $quiz)
	{
		$this->quiz = $quiz;
	}

	/**
	 * Display soal kuisioner beserta jawaban siswa untuk
	 * dikoreksi dan diberi nilai.
	 * 
	 * @param  Int              $id        
	 * @param  Int              $student_id
	 * @param  OfficeExcel      $excel     
	 * @param  QuestionsControl $questions 
	 * @return \Illuminate\View\View                
	 */
	public function show(Int $id, Int $student_id, OfficeExcel $excel, QuestionsControl $questions)
	{
		$quiz = $this->quiz->findWithLessonsAndStudent($id, $student_id);
        $student_quiz = $excel->load($quiz->students[0]->file_path);
        $questions = $questions->from($quiz->file_path, $excel)->get();

        $studentAnswers = [
            'id'        => $student_id,
            'mtp_score' => (int) $quiz->students[0]->multiple_scores,
        ];

        if ($student_quiz->getSheetCount() == 2) {
        	$studentAnswers['multiple']  = $student_quiz->toArray()[0];
        	$studentAnswers['essay']  = $student_quiz->toArray()[1];
        }
        else {
        	if ($student_quiz->getSheetByName('MultipleChoice')) {
        		$studentAnswers['multiple']  = $student_quiz->toArray();
        	}
        	else {
        		$studentAnswers['multiple']  = [];
        	}

        	if ($student_quiz->getSheetByName('Essay')) {
        		$studentAnswers['essay']  = $student_quiz->toArray();
        	}
        	else {
        		$studentAnswers['essay']  = [];
        	}
        }

        return view(dashboard("quiz", "show"), compact('quiz', 'questions', 'studentAnswers'));
	}

	/**
	 * Perbarui nilai kuisioner siswa tipe esai kedalam
	 * database tabel el_student_quizs.
	 *  
	 * @param  Int                $id      
	 * @param  StoreScoreStudents $request 
	 * @param  QuestionsControl   $question
	 * @return \Illuminate\Http\RedirectResponse                    
	 */
	public function update(Int $id, StoreScoreStudents $request, QuestionsControl $question)
	{
		$request->validated();

		$student_quiz = \DB::table(
			'el_student_quizs'
		)->select(
			'file_path'
		)->where(
			'student_id', $request->student_id
		)->first();
		$scores = $request->essay_scores;
		$path = implode('/', array_except(explode('/', $student_quiz->file_path), 6));

		Excel::load($student_quiz->file_path, function ($xlsx) use ($scores) {
			$rows = $xlsx->toArray();
			$sheet = $xlsx->sheet('Essay');
			foreach ($scores as $i => $score) {
				$seq = (int) substr($i, 2, 2);
				$sheet->row($seq+1, [
					$rows[$seq-1]['nomor'],
					$rows[$seq-1]['answers'],
					$rows[$seq-1]['answers_key'],
					$score
				]);
			}
		})->save('xlsx', $path);

        $this->quiz->updateStudentEssayScores($question, $id, $request->except("_token"));

        return redirect()->route(
        	"quiz.detail", $id
        )->with(
        	"update", trans('confirmation.update_score', ['data' => "Kuis"])
    	);
	}
}
