<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\{
	ORM_StudentRepository,
	ORM_NewsRepository,
	ORM_TaskRepository,
	ORM_MaterialRepository,
	ORM_QuizRepository
};

class StudentController extends Controller
{
	protected $student;

	protected $news;

	protected $task;

	protected $quiz;

	public function __construct(
		ORM_StudentRepository $student,
		ORM_NewsRepository $news,
		ORM_TaskRepository $task,
		ORM_MaterialRepository $material,
		ORM_QuizRepository $quiz
	) {
		$this->student = $student;
		$this->news = $news;
		$this->task = $task;
		$this->material = $material;
		$this->quiz = $quiz;

		$this->middleware("auth:student");
	}

    public function home()
    {
    	$id = $this->student->getId();
    	$class = $this->student->getClass();
    	$pararel = $this->student->getPararel();

    	$news = $this->news->findWhereReceiver('Common');

    	$quizs = [
			'today' => $this->quiz->getTodayByClassRoomAndPararel($class, $pararel),
			'total' => $this->quiz->findByClassRoomAndPararel($class, $pararel)->count()
    	];

		$tasks = [
			'today' => $this->task->getTaskTodayByClassRoomAndPararel($class, $pararel),
			'unCollect' => $this->getTaskHaveNotCollected($id, $class, $pararel),
			'total' => $this->task->findByClassRoomAndPararel($class, $pararel)->count()
		];
		
		$materials = $this->material->findByClassRoomAndPararel($class, $pararel);
		
    	return view("dashboard.student.home", 
    		compact(
    			'news',
    			'materials',
    			'tasks',
    			'quizs'
    		)
    	);
    }

    public function getTaskHaveNotCollected(int $id, string $class, string $pararel)
    {
    	$count = 0;
    	$tasks = $this->task->getWhereStudentHaveNotCollected($id, $class, $pararel);

    	foreach ($tasks as $task) {
    		
    		if (empty($task->studens)) {
				
				$count++;
    		}
    	}

    	return $count;
    }
}
