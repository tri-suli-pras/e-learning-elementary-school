<?php

namespace App\Http\Controllers\Data;

use Illuminate\Http\Request;
use App\Rules\ScoreNotMoreThanOneHundred;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquent\ORM_TaskRepository;
use App\Http\Controllers\Data\Traits\ClassroomPararel;
use App\Http\Controllers\Data\Contracts\GetProperties;
use App\Services\File\FileControll;
use App\Repositories\Eloquent\Criteria\{EagerLoad, Teacher, Select, Join};

class TaskController extends Controller implements GetProperties
{
    use ClassroomPararel;

    /**
     * @property [#] $task <Task Model>
     */
    protected $task;
    
    /**
     * 
     * @param \App\Repositories\Eloquent\ORM_TaskRepository $task
     * @return void 
     */
    public function __construct(ORM_TaskRepository $task)
    {
        $this->task = $task;
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     * @return Array <\Illuminate\Http\Request>
     */
    public function getProperties(Request $request) : Array
    {
        return [
            'per_year_id'     => school_year()->id,
            'semester_id'     => \DB::table('semester')->where('status', 'aktif')->first()->id,
            'user_id'     => $request->user_id,
            'title'       => ucfirst($request->title),
            'description' => $request->description,
            'class_room'  => $this->getClassRoom($request->class_room),
            'pararel'     => $this->getPararel($request->class_room),
            'lesson_id'   => $request->lesson_id,
            'deadline'    => \Carbon\Carbon::parse($request->datetime)->toDateTimeString(),
            'file_path'   => $request->file_path
        ];
    }
    
    /**
     * Display list data materi berdasarkan tahun ajaran dan semester.
     * 
     * @param  Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Request query string url.
        $p = $request->periode; $s = $request->semester;

        $tasks = $this->task->withCriteria([
            new Teacher,
            new Join('tahun_ajaran.id', 'per_year_id'),
            new Join('semester.id', 'semester_id'),
            new EagerLoad([
                'lessons' => function ($l) {
                    $l->select(['id', 'nama_matapelajaran']);
                }
            ]),
            new Select([
                'el_tasks.id', 'user_id', 'per_year_id', 
                'lesson_id', 'title', 'tahun_ajaran as year', 
                'pararel', 'semester_id', 'tahun_ajaran.status as periode_status', 
                'class_room',
            ]),
        ]);

        if (auth()->guard('web')->check()) {
            $tasks = $tasks->wheres(['user_id = ' . management()->getId()]);
        }
        else {
            $tasks = $tasks->wheres([
                'class_room = ' . student()->getClass(),
                'pararel = ' . student()->getPararel()
            ]);
        }

        // Jika filter periode tahun dan semester
        if ($p && $s) {
            $tasks = $tasks->wheres([
                "semester_id = $s", 
                "tahun_ajaran.tahun_ajaran = " . periode($p),
            ]);
        }
        else if ($p || $s) {
            if ($p) // Jika filter hanya periode tahun 
                $tasks = $tasks->wheres(['tahun_ajaran.tahun_ajaran = ' . periode($p)]);

            if ($s) // Jika filter hanya periode tahun 
                $tasks = $tasks->wheres(["semester_id = $s", 'tahun_ajaran.status = aktif']);
        }
        else {
            $tasks = $tasks->wheres(['tahun_ajaran.status = aktif', 'semester.status = aktif']);
        }
        // dd($tasks->all()->toArray());
        return view(dashboard("task", "index"), [
            'datas' => $tasks->all()
        ]);
    }
    
    /**
     * Task create page
     * 
     * @return \Illuminate\View\View
     */
    public function create()
    {
    	return view(dashboard("task", "storeORupdate"));
    }
    
    /**
     * Store Task
     *
     * @param \Illuminate\Http\Request $request 
     * @return \Illuminate\Http\RedirectResponse 
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->task->getRules());

        $properties = $this->getProperties($request);

        if ($request->hasFile('file_path')) {
            
            $class = $properties['class_room'];
            $pararel = $properties['pararel'];
            $lid = $properties['lesson_id'];

            $pathToFile = "public/management/task/{$class}{$pararel}/lid_$lid";
            $this->task->createWithFileAndStudents($properties, $pathToFile);
        }
        else
    	   $this->task->createWithStudents($properties);

		return to_route_with_flag(
            "task", 
            "store", 
            trans('confirmation.store', ['data' => "Tugas"]));
    }

    public function storeStudentTask(Request $request, $taskId)
    {
        $this->validate($request, [
            "file_path" => "required|file|mimes:pdf,docx,doc,ppt,pptx"
        ]);

        $this->task->createStudentTask($taskId, $request->file_path);

        return comeback_with_flag("update", "File Tugas Berhasil Diupload");
    }

    public function detail(Request $request, $id)
    {   
        $management = auth()->guard('web');
        $student = auth()->guard('student');
		
        if ($management->check()) {

            $task = $this->task->findTaskWithLessonAndStudents($id);
        }

        if ($student->check()) {
            
            $student = student()->findTasks($id);
            if (empty($student->tasks->toArray()))
                return to_route_with_flag("student.task", "undefined", "Tugas Telah Diedit, Hubungi Guru untuk informasi lebih lanjut");
            
            return view(dashboard("task", "detail"), compact('student'));
            
        }

        return view(dashboard("task", "detail"), compact('task', 'student'));
    }

    public function edit($id)
    {
    	$task = $this->task->find($id);

    	return view(dashboard("task", "storeORupdate"), compact('task'));
    }

    public function update($id, Request $request)
    {
        // Get rules of task model
        $rules = $this->task->getRules();

        // extract request
        $properties = $this->getProperties($request);

        // find task by id must to update
        $task = $this->task->find($id);

        $oldpath = $task->file_path;
        $classroom = $properties['class_room'];
        $cls = $classroom.$properties['pararel'];
        $lesson = $properties['lesson_id'];

        // make new path for incoming file
        $newpath = "public/management/task/$cls/lid_$lesson/";
        
        if ($request->hasFile('file_path')) {
            
            $this->validate($request, $rules);

            $this->task->updateWithCreateNewFile($id, $properties, $oldpath, $newpath);
        }
        else {
            
            $this->validate($request, array_except($rules, ['file_path']));
         
            if ($this->isTaskHasOldFile($oldpath)) {
                
                $filename = explode('/', $oldpath)[5];
                $newpath = str_replace("public", "storage", $newpath) . "$filename";

                $this->task->updateWithMoveFile($id, $properties, $oldpath, $newpath);
            }
            else
                $this->task->updateWithSyncStudents($id, $properties);
        }

        return to_route_with_param_and_flag(
            "task.detail", 
            $id, 
            "update", 
            trans('confirmation.update', ['data' => "Tugas"])
        );
    }

    public function updateStudentScore($id, Request $request)
    {
        $this->validate($request, [
            'scores.*.scores' => [
                'nullable',
                'numeric',
                new ScoreNotMoreThanOneHundred
            ]
        ]);

        $this->task->updateStudentScores($request->except("_token", "_method"));

        return to_route_with_param_and_flag(
            "task.detail", 
            $id, 
            "update_score", 
            trans('confirmation.update_score', ['data' => "Tugas"]));
    }

    public function updateStudentScoreStatus(int $quizORtaskId, int $stdId)
    {
        $isAccepted = $this->task->updateStudentScoreStatus($quizORtaskId, $stdId);
        
        return $isAccepted ? 
                response()->json(trans('confirmation.update_lock', ['data' => "Tugas"]), 200) :
                response()->json("Oops!, Terjadi Kesalahan");
    }
    
    /**
     * @return Boolean
     */
    protected function isTaskHasOldFile($pathToFile)
    {
        return not_null($pathToFile) ;
    }
    
    /**
     * 
     */
    public function delete($id)
    {
    	$this->task->deleteWithFile($id);
        
		return to_route_with_flag("task", "delete", "Tugas Berhasil Dihapus");
    }
}
