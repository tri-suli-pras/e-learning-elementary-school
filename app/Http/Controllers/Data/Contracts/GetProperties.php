<?php 

namespace App\Http\Controllers\Data\Contracts;

use Illuminate\Http\Request;

interface GetProperties
{
	/**
	 * Extract request data
	 * 
	 * @param \Illuminate\Http\Request $request 
	 * @return Array 
	 */
	public function getProperties(Request $request) : Array;
}