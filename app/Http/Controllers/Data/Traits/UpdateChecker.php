<?php 

namespace App\Http\Controllers\Data\Traits;

trait UpdateChecker
{
	protected $model;

	protected $changes;

	final public function __construct(object $model)
	{
		$this->model = $model;
		$this->changes = [];
	}

	private function checkChanges($id, array $newProperties) 
    {
		$oldProperties = $this->model->find($id)->toArray();
		
		foreach ($oldProperties as $key => $oldProp) {
			
			if (array_key_exists($key, $newProperties)) {
				
				if ($newProperties[$key] != $oldProp) {
					
					$this->changes[$key] = $newProperties[$key];
				}
			}
		}

		return $this;
    }

    public function get()
    {
    	return $this->changes;
    }
}