<?php 

namespace App\Services\Quiz\Contratcs;

interface Randomize
{
	/**
	 * make questions to random
	 * @return self
	 */
	public function randomize();
}