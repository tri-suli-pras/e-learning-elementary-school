<?php 

namespace App\Services\Quiz\Contratcs;

use App\Repositories\Eloquent\ORM_QuizRepository;

interface Correction
{
	/**
	 * Correction quiz multiple choice
	 * @param \App\Repositories\Eloquent\ORM_QuizRepository $quiz <Quiz Model>
	 * @param Array $answers
	 * @param $id <quiz id>
	 * @return void 
	 */
	public function correctionMultipleQuestions(ORM_QuizRepository $quiz, array $answers, $id);

	/**
	 * Correction quiz essay
	 * @param \App\Repositories\Eloquent\ORM_QuizRepository $quiz <Quiz Model>
	 * @param Array $answers
	 * @param $id <quiz id>
	 * @return void 
	 */
	public function correctionEssayQuestions(ORM_QuizRepository $quiz, array $answers, $id);
}