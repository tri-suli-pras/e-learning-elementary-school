<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class EmployeeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // employee share variable
        View::composer(
            [
                "dashboard.management.quiz.index",
                "dashboard.management.quiz.storeORupdate",
            ], 
            'App\Http\ViewComposers\EmployeeViewComposer'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
