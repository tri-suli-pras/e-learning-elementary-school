<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Contratcs\{
    ORM_UserInterface,
    ORM_ManagementInterface,
    ORM_StudentInterface,
    ORM_NewsInterface,
    ORM_MaterialInterface,
    ORM_ClassRoomInterface,
    ORM_LessonInterface,
    ORM_QuizInterface,
    ORM_TaskInterface
};
use App\Repositories\Eloquent\{
    ORM_UserRepository,
    ORM_StudentRepository,
    ORM_NewsRepository,
    ORM_EmployeeRepository,
    ORM_MaterialRepository,
    ORM_ClassRoomRepository,
    ORM_LessonRepository,
    ORM_QuizRepository,
    ORM_TaskRepository
};

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     * first arg is the interface
     * second arg is the concrete implementation of repositories
     * @return void
     */
    public function boot()
    {
        $this->app->bind(ORM_UserInterface::class, ORM_UserRepository::class);

        $this->app->bind(ORM_StudentInterface::class, ORM_StudentRepository::class);

        $this->app->bind(ORM_ManagementInterface::class, ORM_EmployeeRepository::class);

        $this->app->bind(ORM_ClassRoomInterface::class, ORM_ClassRoomRepository::class);

        $this->app->bind(ORM_MaterialInterface::class, ORM_MaterialRepository::class);

        $this->app->bind(ORM_LessonInterface::class, ORM_LessonRepository::class);

        $this->app->bind(ORM_QuizInterface::class, ORM_QuizRepository::class);

        $this->app->bind(ORM_TaskInterface::class, ORM_TaskRepository::class);

        $this->app->bind(ORM_NewsInterface::class, ORM_NewsRepository::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
