<?php 

namespace App\Repositories\Contracts;

interface ORM_UserInterface
{
	public function maxId();

	public function loggedIn();

	public function getId();

	public function getName();

	public function getMail();

	public function getSchedule();

	public function getTasks();

	public function getQuizs();

	public function lessons($relations);
}