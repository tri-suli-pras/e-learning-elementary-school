<?php 

namespace App\Repositories\Contracts;

interface ORM_TaskInterface
{	
	/**
	 * Create task and including students relation
	 * and save the task file
	 * if file is present
	 * 
	 * @param Array $properties [\illuminate\Http\Request]
	 * @param String $path 
	 * @return \App\Models\Data\Task 
	 */
	public function createWithFileAndStudents(array $properties, string $path);

	/**
	 * Update task and delete the oldfile 
	 * if the incoming request has a new file
	 * 
	 * @param $id <Task ID>
	 * @param Array $properties [\illuminate\Http\Request]
	 * @param String $oldpath <this will be deleted>
	 * @param String $newpath <store the new one after deleted the oldpath>
	 * @return \App\Models\Data\Task 
	 */
	public function updateWithCreateNewFile($id, array $properties, $oldpath, string $newpath);

	/**
	 * Update task and move the oldfile 
	 * if oldfile exists
	 * 
	 * @param $id <Task ID>
	 * @param Array $properties [\illuminate\Http\Request]
	 * @param String $oldpath <this will be deleted>
	 * @param String $newpath <store the new one after deleted the oldpath>
	 * @return \App\Models\Data\Task 
	 */
	public function updateWithMoveFile($id, array $properties, string $oldpath, string $newpath);

	public function createStudentTask($taskId, array $properties);

	public function updateStudentScores(array $properties);

	/**
	 * Delete Task with that file
	 * 
	 * @param $id <Quiz ID>
	 * @return App\Models\Data\Quiz
	 */
	public function deleteWithFile($id);
	
	/**
	 * Find Task with lesson name
	 * and with students scores
	 * 
	 * @param $id <Quiz ID>
	 * @return \App\Models\Data\Task 
	 */
	public function findTaskWithLessonAndStudent(int $id, int $studentId);
	
	/**
	 * @return \App\Models\Data\Task 
	 */
	public function getAll();
}