<?php 

namespace App\Repositories\Eloquent;

use DB;
use App\Repositories\BaseRepository;
use App\Repositories\Contracts\ORM_ManagementInterface;
use App\Models\Management\{Employee, UserTeacher};
use App\Repositories\Eloquent\Criteria\{EagerLoad, OrderBy};

class ORM_EmployeeRepository extends BaseRepository implements ORM_ManagementInterface
{
	public function entity()
	{
		return Employee::class;
	}

	public function selectNameAndId()
	{
		return $this->Entity->select('id', 'nama_pegawai AS name');
	}

	public function updateProfile($id, array $properties)
	{
		$properties['ttl'] = $properties['tempat']. ',' .$properties['ttl'];
		unset($properties['tempat']);
		return $this->update($id, $properties);
	}

	public function updateWithImageFile(int $id, array $properties)
	{
		$properties['ttl'] = $properties['tempat']. ',' .$properties['ttl'];
		unset($properties['tempat']);
		return $this->update($id, $properties);
	}

	public function findWithAccount($id)
	{
		return $this->withCriteria([
			
			new EagerLoad(['user_teacher' => function($query) use ($id) {
				
				$query->with(['account'])->where('pegawai_id', $id);
			}])
		])->find($id);
	}
}