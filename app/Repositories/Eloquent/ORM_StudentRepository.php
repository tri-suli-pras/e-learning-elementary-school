<?php 

namespace App\Repositories\Eloquent;

use App\Models\Actors\Student;
use App\Repositories\BaseRepository;
use App\Repositories\Contracts\ORM_StudentInterface;
use App\Repositories\Eloquent\Criteria\{EagerLoad, Join, Teacher, Select};

class ORM_StudentRepository extends BaseRepository implements ORM_StudentInterface
{
	public function entity()
	{
		return Student::class;
	}

	public function findNews()
	{
		return $this->withCriteria([
			new EagerLoad(['news' => function ($news) {
				$news->with(["teachers" => function ($query) {
					$query->with(["employees" => function ($query) {
						$query->join("pegawai", "user_pegawai.pegawai_id", '=', "pegawai.id");
					}]);
				}]);
			}])
		])->find($this->getId());
	}

	public function findTasks(int $taskID)
	{
		return $this->withCriteria([
			new EagerLoad(['tasks' => function ($tasks) use ($taskID) {
				$tasks->with(["teachers" => function ($query) {
					$query->with(["employees" => function ($query) {
						$query->join("pegawai", "user_pegawai.pegawai_id", '=', "pegawai.id");
					}]);
				}, 'lessons'])->select(
					'el_tasks.*',
					'el_student_tasks.file_path AS student_file_path',
					'el_student_tasks.status AS student_status',
					'el_student_tasks.scores AS student_score'
				)->find($taskID);
			}])
		])->find($this->getId());
	}

	public function updateWithImageFile(int $id, array $properties)
	{
		return $this->update($id, $properties);
	}

	public function getRules()
	{
		return $this->Entity::RULE;
	}

	public function select(array $fields)
	{
		return $this->withCriteria([
			new Select($fields)
		]);
	}

	public function getName()
	{
		return $this->authStudent()->nama_siswa;
	}

	public function getNisn()
	{
		return $this->authStudent()->nisn;
	}

	public function getClass()
	{
		return $this->authStudent()->kelas;
	}

	public function getPararel()
	{
		return $this->authStudent()->pararel;
	}

	public function getId()
	{
		return $this->authStudent()->id;
	}

	public function getAdministration($id)
	{
		return $this->withCriteria([
			
			new EagerLoad(["administrations" => function ($query) {
				
				$query->where('tahun', date('Y'));
			}])
		])->find($id);
	}

	public function getNews($status)
	{
		return $this->withCriteria([
			new EagerLoad(['news' => function($news) use ($status) {
				$news->with(["teachers" => function($teachers) {
					$teachers->with(["employees" => function ($employees) {
						$employees->join("pegawai", "user_pegawai.pegawai_id", '=', "pegawai.id");
					}]);
				}])->where('status', $status); 
			}])
		])->find($this->getId());
	}

	public function findStudentQuiz($quizId, $studentId)
	{
		return $this->withCriteria([
			new EagerLoad(["quizs" => function($query) use ($quizId) {
				$query->with(['lessons'])->where('quiz_id', $quizId)->first();
			}])
		])->find($studentId);
	}

	public function showTask($taskID)
	{
		return $this->withCriteria([
			new EagerLoad(["tasksResult" => function($query) use ($taskID) {
				
				$query->select('scores')->where('task_id', $taskID);
			}]),
			new EagerLoad(["tasks" => function ($query) use ($taskID) {
				$query->with(["lessons"])->where('id', $taskID);
			}])
		])->find($this->getId());
	}


	protected function authStudent()
	{
		return auth()->guard("student")->user();
	}
}