<?php 

namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\Contracts\Criteria\CriterionInterface;

class ClassRoom implements CriterionInterface
{
	protected $room;

	public function __construct($room)
	{
		$this->room = $room;
	}

	public function apply($entity)
	{
		return $entity->with(["classes" => function ($query){
						
				$query->where('nama_kelas', $this->room);
			}])
		;
	}
}