<?php 

namespace App\Repositories;

use Closure;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Contracts\BaseRepositoryInterface;
use App\Repositories\Contracts\Criteria\CriteriaInterface;

class BaseRepository implements BaseRepositoryInterface, CriteriaInterface
{
	protected $Entity;

	public function __construct()
	{
		$this->Entity = $this->resolveEntity();
	}

	public function all()
	{
		return $this->Entity->get();
	}

	public function find($id)
	{
		return $this->Entity->find($id);
	}

	public function findWhere($colums, $value)
	{
		return $this->Entity->where($colums, $value)->get();
	}

	public function findAndWhere($column1, $value1, $colum2, $value2)
	{
		return $this->Entity->where($column1, $value1)->where($colum2, $value2)->get();
	}

	public function findAndWhereWithNotEqual($column1, $value1, $colum2, $value2)
	{
		return $this->Entity->where($column1, $value1)->where($colum2, '!=', $value2)->get();
	}

	public function findAndWhereFirst($column1, $value1, $colum2, $value2)
	{
		return $this->Entity->where($column1, $value1)->where($colum2, $value2)->first();
	}

	public function findWhereFirst($colums, $value)
	{
		return $this->Entity->where($colums, $value)->first();
	}

	public function findWherePaginate($colums, $value, $page)
	{
		return $this->Entity->where($colums, $value)->paginate($page);
	}

	public function findAndWherePaginate($column1, $value1, $colum2, $value2, $page)
	{
		return $this->Entity->where($column1, $value1)->where($colum2, $value2)->paginate($page);
	}

	public function findAndWherePaginateWithIn($column1, $value1, $colum2, $value2, $in, array $value, $page)
	{
		return $this->Entity->where($column1, $value1)->where($colum2, $value2)->whereIn($in, $value)->paginate($page);
	}

	public function findWheresWithNotIn($column1, $value1, $colum2, $value2, $in, array $value)
	{
		return $this->Entity->where($column1, $value1)->where($colum2, $value2)->whereNotIn($in, $value);
	}

	public function findWhereNotIn($column, array $value)
	{
		return $this->Entity->whereNotIn($column, $value);
	}

	public function findWhereWithNotIn($column, $value, $columns, $values)
	{
		return $this->Entity->where($column, $value)->whereNotIn($columns, $values)->get();
	}

	public function findWithPaginate($id, $perPage = 10)
	{
		return $this->Entity->find($id)->paginate($perPage);
	}

	public function findAndDelete($column, $value)
	{
		return $this->Entity->where($column, $value);
	}

	public function itHas($relations = null, Closure $then)
	{
		return $this->Entity::whereHas($relations, $then);
	}

	public function paginate($perPage = 10)
	{
		return $this->Entity->paginate($perPage);
	}

	public function create(array $properties)
	{
		return $this->Entity->create($properties);
	}

	public function update($id, array $properties)
	{
		return $this->find($id)->update($properties);
	}

	public function updateWithFile($id, array $properties, string $path)
	{

		return $this->update($id, $properties);
	}

	public function delete($id)
	{
		return $this->find($id)->delete();
	}

	public function wheres(Array $keyValuePairs)
	{
		foreach ($keyValuePairs as $key => $condition) {
			$cond = explode(' ', $condition);
			$col_key = $cond[0]; $opt = $cond[1]; $col_val = $cond[2];

			$this->Entity = $this->Entity->where($col_key, $opt, $col_val);
		}

		// dd($this->Entity->toSql());

		return $this;
	}

	public function withCriteria(...$criterias)
	{
		$criterias = array_flatten($criterias);
		
		foreach ($criterias as $criteria)
		{
			$this->Entity = $criteria->apply($this->Entity);
		}

		return $this;
	}

	private function resolveEntity()
	{

		if (! method_exists($this, 'entity'))
		{
			throw new NoEntityDefined();
		} 

		return app()->make($this->entity());
		
		/*else {
			if (is_array($this->entity())) 
			{
				$Entities = [];

				foreach ($this->entity() as $entities)
				{
					$Entities[strtolower(explode("\\", $entities)[3])] = app()->make($entities);
				}

				return $Entities;
			} 
			else {
				
			}
		}*/
	}
}