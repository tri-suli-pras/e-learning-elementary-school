<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class InvalidDay implements Rule
{
    protected $maxDay;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->maxDay = 31;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (int) $value > 0 && $value <= $this->maxDay;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.invalid_date', ['what' => "Hari"]);
    }
}
