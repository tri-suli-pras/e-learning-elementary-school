<?php 

////////////////////////////////////////////////////
// Contoh format soal mata pelajaran Tematik PJOK //
////////////////////////////////////////////////////

return [
	'multiple_choice' => [
		[
			'question' 	 => 'Benda yang ditangkap dan dilempar adalah ...',
			'option'	 => [
				'a' => 'Kelapa',
				'b' => 'Roti',
				'c' => 'Bola',
				'd' => 'Kasur'
			],
			'answer_key' => 'c'
		],
		[
			'question' 	 => 'Pada saat berjalan, pandangan ke ...',
			'option'	 => [
				'a' => 'Samping',
				'b' => 'Belakang',
				'c' => 'Depan',
				'd' => 'Tetap'
			],
			'answer_key' => 'c'
		],
		[
			'question' 	 => 'Yang termasuk latihan dasar kekuatan otot lengan dan tungkai adalah...',
			'option'	 => [
				'a' => 'Jalan jinjit',
				'b' => 'Mendorong tembok',
				'c' => 'Berdiri satu kaki',
				'd' => 'Berdiri dua kaki'
			],
			'answer_key' => 'c'
		],
		[
			'question' 	 => 'Gambar disamping adalah latihan...',
			'option'	 => [
				'a' => 'Loncat ke depan',
				'b' => 'Loncat ke samping',
				'c' => 'Loncat ke belakang',
				'd' => 'Loncat ke mana saja'
			],
			'answer_key' => 'a'
		],
		[
			'question' 	 => 'Tujuan Senam Ria Anak Indonesia adalah...',
			'option'	 => [
				'a' => 'melatih keterampilan gerak dasar',
				'b' => 'melatih kekuatan',
				'c' => 'melatih kelincahan',
				'd' => 'Melatih tubuh'
			],
			'answer_key' => 'a'
		],
		[
			'question' 	 => 'Rambut yang kotor menyebabkan kulit kepala menjadi...',
			'option'	 => [
				'a' => 'Bersih',
				'b' => 'Merah',
				'c' => 'Gatal',
				'd' => 'Garing'
			],
			'answer_key' => 'c'
		],
		[
			'question' 	 => 'Melakukan gerakan back up kedua tangan di...',
			'option'	 => [
				'a' => 'Dada',
				'b' => 'Kepala',
				'c' => 'Punggung',
				'd' => 'Kaki'
			],
			'answer_key' => 'b'
		],
		[
			'question' 	 => 'Gambar disamping adalah anak sedang melakukan...',
			'option'	 => [
				'a' => 'Lompat Tinggi',
				'b' => 'Lompat tali',
				'c' => 'Lompat jauh',
				'd' => 'Lompatan'
			],
			'answer_key' => 'b'
		],
		[
			'question' 	 => 'Berlatih renang untuk anak-anak di kolam yang...',
			'option'	 => [
				'a' => 'Dalam',
				'b' => 'Dunkar',
				'c' => 'Dangkal',
				'd' => null
			],
			'answer_key' => 'c'
		],
		[
			'question' 	 => 'Gambar disamping adalah anak sedang melatih otot ...',
			'option'	 => [
				'a' => 'Tangan',
				'b' => 'Pinggang',
				'c' => 'Punggung',
				'd' => 'Kaki'
			],
			'answer_key' => 'b'
		],
	],
	'essay'		=> [
		[
			'question'	=> 'Bagaimanakah gerakan kaki dan tangan saat berjalan ?',
			'answer_key'	=> 'kaki melangkah ke depan dan tangan berayun.'
		],
		[
			'question'	=> 'Apakah singkatan dari SRAI ?',
			'answer_key'	=> 'Senam Ria Anak Indonesia'
		],
		[
			'question'	=> 'Sebutkan  bentuk-bentuk latihan kekuatan otot dada dan punggung !',
			'answer_key'	=> 'Sit Up Back Up'
		],
		[
			'question'	=> 'Sebutkan manfaat senam irama bagi tubuh kita ?',
			'answer_key'	=> 'untuk menjaga kesehatan tubuh agar lebih kuat.'
		],
		[
			'question'	=> 'Kapan kita mencuci tangan',
			'answer_key'	=> 'ketika mau makan'
		],
	]
];