<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPerYearIdColumnElQuizsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('el_quizs', function (Blueprint $table) {
            $table->unsignedInteger('per_year_id')->after("id");

            $table->foreign("per_year_id")
                  ->references('id')
                  ->on("tahun_ajaran")
                  ->onUpdate("NO ACTION")
                  ->onDelete("RESTRICT");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('el_quizs', function (Blueprint $table) {
            $table->dropColumn('per_year_id');
        });
    }
}
