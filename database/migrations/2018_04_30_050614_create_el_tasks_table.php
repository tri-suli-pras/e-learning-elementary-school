<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('el_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id")->unsigned();
            $table->integer("lesson_id")->unsigned();
            $table->char("class_room", 2);
            $table->char("pararel", 2);
            $table->string("title", 75);
            $table->mediumText("description");
            $table->dateTime("deadline");
            $table->string("status", 10)->default("unlock");
            $table->string("file_name", 75);
            $table->string("file_path", 150);
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onUpdate('NO ACTION')
                  ->onDelete('NO ACTION');

            $table->foreign('lesson_id')
                  ->references('id')
                  ->on('matapelajaran')
                  ->onUpdate('NO ACTION')
                  ->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('el_tasks');
    }
}
