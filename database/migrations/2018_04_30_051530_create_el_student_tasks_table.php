<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElStudentTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('el_student_tasks', function (Blueprint $table) {
            $table->integer("task_id")->unsigned();
            $table->integer("student_id")->unsigned();
            $table->string("file_path", 150)->nullable();
            $table->char("scores", 4)->nullable();
            $table->string("status", 10)->nullable()->default("unlock");

            $table->foreign("task_id")
                  ->references('id')
                  ->on("el_tasks")
                  ->onUpdate("NO ACTION")
                  ->onDelete("CASCADE");

            $table->foreign("student_id")
                  ->references('id')
                  ->on("siswas")
                  ->onUpdate("NO ACTION")
                  ->onDelete("NO ACTION");

            $table->primary(["task_id", "student_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('el_student_tasks');
    }
}
