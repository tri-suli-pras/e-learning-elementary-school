<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSemesterIdColumnElTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('el_tasks', function (Blueprint $table) {
            $table->unsignedInteger('semester_id')->after("per_year_id");

            $table->foreign("semester_id")
                  ->references('id')
                  ->on("semester")
                  ->onUpdate("NO ACTION")
                  ->onDelete("RESTRICT");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('el_tasks', function (Blueprint $table) {
            $table->dropColumn('semester_id');
        });
    }
}
