<?php

use Carbon\Carbon as DateTime;
use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile as File;
use App\Repositories\Eloquent\ORM_QuizRepository;

class QuizTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $repository = new ORM_QuizRepository();

        foreach ([
        	[
        		'per_year_id'		=> 3,
        		'semester_id'		=> 1,
        		'user_id'			=> 1,
        		'lesson_id'			=> 33,
        		'class_room'		=> 3,
        		'pararel'			=> 'A',
        		'title'				=> 'Seeder Bahasa Indonesia 1',
        		'description'		=> str_random(50),
        		'starting_time' 	=> DateTime::create(random_int(2017, 2019), random_int(1, 12), random_int(1, 28), random_int(8, 15), random_int(0, 60), random_int(0, 60), 'Asia/Jakarta'),
        		'count_down'		=> 60,
        		'weight_essay'  	=> 30,
        		'weight_multiple'	=> 70,
        		'file_path'			=> $this->file('bahasa_indonesia'),
        	],
        	[
        		'per_year_id'		=> 3,
        		'semester_id'		=> 2,
        		'user_id'			=> 1,
        		'lesson_id'			=> 33,
        		'class_room'		=> 3,
        		'pararel'			=> 'A',
        		'title'				=> 'Seeder Bahasa Indonesia 2',
        		'description'		=> str_random(50),
        		'starting_time' 	=> DateTime::create(random_int(2017, 2019), random_int(1, 12), random_int(1, 28), random_int(8, 15), random_int(0, 60), random_int(0, 60), 'Asia/Jakarta'),
        		'count_down'		=> 90,
        		'weight_essay'  	=> 70,
        		'weight_multiple'	=> 30,
        		'file_path'			=> $this->file('bahasa_indonesia'),
        	],
        	[
        		'per_year_id'		=> 1,
        		'semester_id'		=> 1,
        		'user_id'			=> 6,
        		'lesson_id'			=> 1,
        		'class_room'		=> 2,
        		'pararel'			=> 'A',
        		'title'				=> 'Seeder Tematik PJOK 1',
        		'description'		=> str_random(50),
        		'starting_time' 	=> DateTime::create(random_int(2017, 2019), random_int(1, 12), random_int(1, 28), random_int(8, 15), random_int(0, 60), random_int(0, 60), 'Asia/Jakarta'),
        		'count_down'		=> 60,
        		'weight_essay'  	=> 50,
        		'weight_multiple'	=> 50,
        		'file_path'			=> $this->file('tematik_pjok'),
        	],
        	[
        		'per_year_id'		=> 1,
        		'semester_id'		=> 2,
        		'user_id'			=> 6,
        		'lesson_id'			=> 1,
        		'class_room'		=> 2,
        		'pararel'			=> 'A',
        		'title'				=> 'Seeder Tematik PJOK 2',
        		'description'		=> str_random(50),
        		'starting_time' 	=> DateTime::create(random_int(2017, 2019), random_int(1, 12), random_int(1, 28), random_int(8, 15), random_int(0, 60), random_int(0, 60), 'Asia/Jakarta'),
        		'count_down'		=> 60,
        		'weight_essay'  	=> 60,
        		'weight_multiple'	=> 40,
        		'file_path'			=> $this->file('tematik_pjok'),
        	],
        	[
        		'per_year_id'		=> 2,
        		'semester_id'		=> 1,
        		'user_id'			=> 7,
        		'lesson_id'			=> 33,
        		'class_room'		=> 1,
        		'pararel'			=> 'A',
        		'title'				=> 'Seeder Bahasa Indonesia 1',
        		'description'		=> str_random(50),
        		'starting_time' 	=> DateTime::create(random_int(2017, 2019), random_int(1, 12), random_int(1, 28), random_int(8, 15), random_int(0, 60), random_int(0, 60), 'Asia/Jakarta'),
        		'count_down'		=> 60,
        		'weight_essay'  	=> 50,
        		'weight_multiple'	=> 50,
        		'file_path'			=> $this->file('bahasa_indonesia'),
        	],
        	[
        		'per_year_id'		=> 2,
        		'semester_id'		=> 2,
        		'user_id'			=> 1,
        		'lesson_id'			=> 1,
        		'class_room'		=> 1,
        		'pararel'			=> 'A',
        		'title'				=> 'Seeder Tematik PJOK 1',
        		'description'		=> str_random(50),
        		'starting_time' 	=> DateTime::create(random_int(2017, 2019), random_int(1, 12), random_int(1, 28), random_int(8, 15), random_int(0, 60), random_int(0, 60), 'Asia/Jakarta'),
        		'count_down'		=> 60,
        		'weight_essay'  	=> 70,
        		'weight_multiple'	=> 30,
        		'file_path'			=> $this->file('tematik_pjok'),
        	],
        ] as $key => $value) {
        	$repository->createThroughFile(
        		$value, $this->pathToFile(
        			"{$value['class_room']}{$value['pararel']}", $value['lesson_id'])
        	);
        }
    }

    /**
     * Path file
     * 
     * @param  String|Int $classname
     * @param  String|Int $lid      
     * @return String
     */
    public function pathToFile($classname, $lid) : String 
    {
    	return "public/management/quiz/$classname/lid_$lid";
    }

    public function file(String $file)
    {
    	return new File(
    		base_path("../../../Documentation/@web-skripsi-baiturrahamn-app/template_soal/both/$file.xlsx"),
    		$file,
    	);
    }
}
