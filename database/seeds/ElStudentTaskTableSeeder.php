<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ElStudentTaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ([
        	[
        		'task_id' 		=> 2,
        		'per_year_id' 	=> 1,
        		'semester_id' 	=> 1,
        		'student_id'	=> 8,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 2,
        		'per_year_id' 	=> 1,
        		'semester_id' 	=> 1,
        		'student_id'	=> 9,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 2,
        		'per_year_id' 	=> 1,
        		'semester_id' 	=> 1,
        		'student_id'	=> 10,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 2,
        		'per_year_id' 	=> 1,
        		'semester_id' 	=> 1,
        		'student_id'	=> 11,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 2,
        		'per_year_id' 	=> 1,
        		'semester_id' 	=> 1,
        		'student_id'	=> 12,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 2,
        		'per_year_id' 	=> 1,
        		'semester_id' 	=> 1,
        		'student_id'	=> 13,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 2,
        		'per_year_id' 	=> 1,
        		'semester_id' 	=> 1,
        		'student_id'	=> 14,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 2,
        		'per_year_id' 	=> 1,
        		'semester_id' 	=> 1,
        		'student_id'	=> 28,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 2,
        		'per_year_id' 	=> 1,
        		'semester_id' 	=> 1,
        		'student_id'	=> 30,
        		'scores'		=> random_int(0, 100)
        	],

        	[
        		'task_id' 		=> 1,
        		'per_year_id' 	=> 2,
        		'semester_id' 	=> 1,
        		'student_id'	=> 8,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 1,
        		'per_year_id' 	=> 2,
        		'semester_id' 	=> 1,
        		'student_id'	=> 9,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 1,
        		'per_year_id' 	=> 2,
        		'semester_id' 	=> 1,
        		'student_id'	=> 10,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 1,
        		'per_year_id' 	=> 2,
        		'semester_id' 	=> 1,
        		'student_id'	=> 11,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 1,
        		'per_year_id' 	=> 2,
        		'semester_id' 	=> 1,
        		'student_id'	=> 12,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 1,
        		'per_year_id' 	=> 2,
        		'semester_id' 	=> 1,
        		'student_id'	=> 13,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 1,
        		'per_year_id' 	=> 2,
        		'semester_id' 	=> 1,
        		'student_id'	=> 14,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 1,
        		'per_year_id' 	=> 2,
        		'semester_id' 	=> 1,
        		'student_id'	=> 28,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 1,
        		'per_year_id' 	=> 2,
        		'semester_id' 	=> 1,
        		'student_id'	=> 30,
        		'scores'		=> random_int(0, 100)
        	],

        	[
        		'task_id' 		=> 5,
        		'per_year_id' 	=> 3,
        		'semester_id' 	=> 2,
        		'student_id'	=> 8,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 5,
        		'per_year_id' 	=> 3,
        		'semester_id' 	=> 2,
        		'student_id'	=> 9,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 5,
        		'per_year_id' 	=> 3,
        		'semester_id' 	=> 2,
        		'student_id'	=> 10,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 5,
        		'per_year_id' 	=> 3,
        		'semester_id' 	=> 2,
        		'student_id'	=> 11,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 5,
        		'per_year_id' 	=> 3,
        		'semester_id' 	=> 2,
        		'student_id'	=> 12,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 5,
        		'per_year_id' 	=> 3,
        		'semester_id' 	=> 2,
        		'student_id'	=> 13,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 5,
        		'per_year_id' 	=> 3,
        		'semester_id' 	=> 2,
        		'student_id'	=> 14,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 5,
        		'per_year_id' 	=> 3,
        		'semester_id' 	=> 2,
        		'student_id'	=> 28,
        		'scores'		=> random_int(0, 100)
        	],
        	[
        		'task_id' 		=> 5,
        		'per_year_id' 	=> 3,
        		'semester_id' 	=> 2,
        		'student_id'	=> 30,
        		'scores'		=> random_int(0, 100)
        	],
        ] as $key => $value) {
        	DB::table('el_student_tasks')->insert($value);
        }
    }
}
