<?php 

Route::group(['middleware' => ["web"]], function () {

	Route::group(['prefix' => "e-learning"], function () {
			
			Route::post('login-siswa', [
				'uses'	=> "AuthStudent\LoginController@authentication",
				'as'	=> "student.auth"
			]);

		Route::group(['middleware' => ["student"]], function () {
			Route::group(['prefix' => "siswa"], function () {
				Route::get('/', [
					'uses'	=> "Dashboard\StudentController@home",
					'as'	=> "student.home"
				]);

				Route::get('profil/{id}', [
					'uses'	=> "Profile\StudentController@show",
					'as'	=> "student.profile"
				]);

				Route::get('edit-profil/{id}', [
					'uses'	=> "Profile\StudentController@edit",
					'as'	=> "student.edit"
				]);

				Route::patch('update-profil/{id}', [
					'uses'	=> "Profile\StudentController@update",
					'as'	=> "student.update"
				]);

				Route::get("logout", [
					'uses' => "AuthStudent\LoginController@logout",
					'as'   => "student.logout"
				]);

				Route::group(['prefix' => "materi"], function () {
					Route::get('/', [
						'uses' => "Data\MaterialController@index",
						'as'   => "student.material"
					]);

					Route::get('list-materi/{lessonId}', [
						'uses' => "Data\MaterialController@detail",
						'as'   => "student.material.detail"
					]);

					Route::get('baca-materi/{id}', [
						'uses' => "Data\MaterialController@show",
						'as'   => "student.material.read"
					]);
				});

				Route::group(['prefix' => "tugas"], function () {
					Route::get('/', [
						'uses' => "Data\TaskController@index",
						'as'   => "student.task"
					]);

					Route::get('detail-tugas/{id}', [
						'uses' => "Data\TaskController@detail",
						'as'   => "student.task.show"
					]);

					Route::post('upload-tugas/{taskId}', [
						'uses' => "Data\TaskController@storeStudentTask",
						'as'   => "student.task.upload"
					]);
				});

				//////////////////////////////////////////////
				// Kontroller untuk handel fitur kuisioner  //
				// untuk user siswa                         //
				//////////////////////////////////////////////
				Route::group(['namespace' => 'Questionnaire', 'prefix' => "kuis"], function ($route) {
					$route->get('/', 'QuizController@index')->name('student.quiz');
					$route->get('detail-kuis/{quiz_id}', 'QuizController@show')->name('student.quiz.detail');
					$route->get('soal-kuis/{quiz_id}', 'StudentController@create')->name('student.quiz.create');
					$route->post('save-kuis/{quiz_id}', 'TeacherController@store')->name('student.quiz.store');
				});

				Route::group(['prefix' => "berita"], function () {
					Route::get('/', [
						'uses' => "Data\NewsController@index",
						'as'   => "student.news"
					]);

					Route::get('detail-berita/{id}', [
						'uses' => "Data\NewsController@detail",
						'as'   => "student.news.detail"
					]);
				});
			});
		});

	});

});