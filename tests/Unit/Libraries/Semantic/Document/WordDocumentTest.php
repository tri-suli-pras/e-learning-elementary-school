<?php

namespace Tests\Unit\Libraries\Semantic\Document;

use App\Libraries\Semantic\Document\WordDocument;
use Tests\TestCase;

class WordDocumentTest extends TestCase
{
	/**
	 * @test
	 * @return 
	 *
	 * @expectedException \InvalidArgumentException
	 */
	public function can_set_a_sentence_as_word_document_with_string_and_array()
	{
		$wordFromString = new WordDocument('Ini', 'adalah', 'dokumen');
		$wordFromArray = new WordDocument(['array', 'dokumen']);

		$this->assertSame(['Ini', 'adalah', 'dokumen'], $wordFromString->getAll());
		$this->assertSame(['array', 'dokumen'], $wordFromArray->getAll());

		$invalidDocument = new WordDocument(1, 'test');
	}

	/**
	 * @test
	 * @return void
	 */
	public function can_add_a_single_new_word_into_the_exists_words()
	{
		$word = new WordDocument('kata', 'awal');
		$word->addWord('tambahan');
		$word->addWord('lagi');

		$this->assertFalse( empty($word->getAllNewWord()) );
		$this->assertSame(
			['kata', 'awal', 'tambahan', 'lagi'],
			$word->getAll()
		);
	}

	/**
	 * @test
	 * @return void
	 */
	public function can_add_a_few_new_word_into_the_exists_words()
	{
		$word = new WordDocument('kata', 'awal');
		$word->addWords(['tambahan', 'lagi']);

		$this->assertTrue( is_array($word->getAllNewWord()) );
		$this->assertSame(
			['kata', 'awal', 'tambahan', 'lagi'],
			$word->getAll()
		);
	}

	/**
	 * @test
	 * @return void
	 */
	public function can_find_words_where_first_character_is_matching()
	{
		$word = new WordDocument(['satu', 'dua', 'tiga', 'saya', 'dan']);
		$startWith_S = $word->findStartWith('s');
		$startWith_D = $word->findStartWith('d');

		$this->assertSame(['satu', 'saya'], $startWith_S);
		$this->assertSame(['dua', 'dan'], $startWith_D);
	}

	/**
	 * @test
	 * @return void
	 */
	public function can_find_words_where_last_character_is_matching()
	{
		$word = new WordDocument(['satu', 'dua', 'tiga', 'saya', 'dan', 'aku']);
		$endWith_A = $word->findEndWith('a');
		$endWith_U = $word->findEndWith('u');

		$this->assertSame(['dua', 'tiga', 'saya',], $endWith_A);
		$this->assertSame(['satu', 'aku'], $endWith_U);
	}
}
