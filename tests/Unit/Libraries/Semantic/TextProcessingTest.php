<?php

namespace Tests\Unit\Libraries\Semantic;

use Tests\TestCase;
use App\Libraries\Semantic\TextPreprocessing;

class TextProcessingTest extends TestCase
{
	/**
	 * @test
	 * @return void
	 */
    public function can_remove_stopword() : Void 
    {
    	$textProcessor = TextPreprocessing::normalize(
    		'1 tambah dua adalah = 3. bos!!!'
    	)->removeStopWord();

    	$this->assertSame('1 3 bos', $textProcessor->getResult());
    }

    /**
     * @test
     * @return void
     */
    public function can_do_stemming_the_given_sentence() : Void
    {
    	$textProcessor = TextPreprocessing::normalize(
    		'Selamat Menikmati HIDangan pembuka.'
    	)->stem();

    	$this->assertSame('selamat nikmat hidang buka', $textProcessor->getResult());
    }

    /**
     * @test
     * @return void
     */
    public function can_normalize_the_text_until_stemming_proses()
    {
    	$textProcessor = TextPreprocessing::normalize(
    		'1 tambah dua adalah = 3. bos!!!. Selamat Menikmati HIDangan pembuka.'
    	)->removeStopWord()->stem();

    	$this->assertSame('1 3 bos selamat nikmat hidang buka', $textProcessor->getResult());
    }
}
