<?php

namespace Tests\Unit\Utils;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HelperFunctionTest extends TestCase
{
	/**
	 * Function school_year()
	 * 
	 * @test
	 * @return Void
	 */
   public function school_year_function_get_first_where_status_active()
   {
   		$result = school_year();

   		$this->assertEquals('2', $result->id);
   		$this->assertEquals('2018/2019', $result->year);
   		$this->assertEquals('aktif', $result->status);
   	}

   	/**
   	 * Function school_year($start, $end, $status)
   	 * 
   	 * @test
   	 * @return Void
   	 */
   	public function school_year_function_get_first_by_params() 
   	{
   		$result = school_year(2017, 2018, 'tidak aktif');

   		$this->assertEquals('1', $result->id);
   		$this->assertEquals('2017/2018', $result->year);
   		$this->assertEquals('tidak aktif', $result->status);
   	}
}
