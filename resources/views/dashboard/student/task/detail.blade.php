@extends("dashboard.student.home")

@push("css")

@endpush

@section("content")
<div class="content-header">
    <div>
        <a href="{{ route('student.task') }}"> 
            <div class="fa fa-angle-double-left mr-2"></div>
            {{ trans('label.index_page_of', ['what' => "Tugas"]) }}
        </a>
    </div>

    <h2 class="title">
        {!! trans('label.detail', ['for' => "Tugas", 'what' => $student->tasks[0]->lessons->nama_matapelajaran]) !!}
    </h2>
    @if (session("update"))
        <div class="right-content" id="session">
            <span>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <span class="lead">{{ trans('confirmation.success') }} {{ session('update') }}</span>
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </span>
        </div>
    @endif
</div>
<div class="content-body">
	<div class="row">
		<div class="col-md-8">
			<dl id="dlRow" class="row">
				<dt class="col-4"><h4>{{ trans('label.for.-T') }}</h4></dt>
				<dd class="col-8">{{ $student->tasks[0]->title }}</dd>

				<dt class="col-4"><h4>{{ trans('label.for.-C') }}</h4></dt>
				<dd class="col-8">{{ "{$student->tasks[0]->class_room} {$student->tasks[0]->pararel}" }}</dd>

				<dt class="col-4"><h4>{{ trans('label.for.-D') }}</h4></dt>
				<dd class="col-8">@if ( null($student->tasks[0]->description) || $student->tasks[0]->description == '' )
                        <b>{{ trans('label.empty') }}</b>
                @else
					{{ $student->tasks[0]->description }}                    
                @endif</dd>

				<dt class="col-4"><h4>{{ trans('label.for.--D') }}</h4></dt>
				<dd class="col-8">{{ \Carbon\Carbon::parse($student->tasks[0]->deadline)->format('d F Y H:i') }}
                </dd>

				<dt class="col-4"><h4>{{ trans('label.task_file') }}</h4></dt>
				<dd class="col-8">
                    @empty ($student->tasks[0]->file_path)
                        <b>{{ trans('label.empty') }}</b>
                    @else
					<a href="{{ asset($student->tasks[0]->file_path) }}" download="{{ $student->tasks[0]->title }}">
						{{ $student->tasks[0]->file_name }}
					</a>
                    @endempty
				</dd>

                <dt class="col-4"><h4>{{ trans('label.for.-s') }}</h4></dt>
                <dd class="col-8">
                    @if (not_null($student->tasks[0]->student_score))
                        <span class="badge badge-success">{{ trans('label.task_status', ['is' => "Sudah"]) }}</span>
                    @elseif (not_null($student->tasks[0]->student_file_path))
                        <span class="badge badge-info">{{ trans('label.task_status', ['is' => "Sudah"]) }}</span>
                    @else
                        <span class="badge badge-danger">{{ trans('label.task_status', ['is' => "Belum"]) }}</span>
                    @endif
                </dd>
                
				<dt class="col-4"><h4>{{ trans('label.for.-O') }}</h4></dt>
				<dd class="col-8">
					<a class="badge badge-info" href="{{ '#scores'.$student->tasks[0]->id }}" data-toggle="modal">{{ trans('label.score') }}</a>
                    @if (null($student->tasks[0]->student_score) && null($student->tasks[0]->student_file_path))
                        <a class="badge badge-info" href="#upload" data-toggle="modal">{{ trans('label.act.upload') }}</a>
                    @endempty
				</dd>
			</dl>
		
			<hr />
		</div>
	</div>
</div>


@endsection

@section("modal")
    <div class="modal fade" id="{{ 'scores'.$student->tasks[0]->id }}" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ $student->tasks[0]->title }}</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close" title="Tutup" data-toggle="tooltip">
                        <img src="{{ asset('web/images/icon-close.png') }}"/>
                    </button>
                </div>
                <div class="modal-body">
                    <h2 class="text-center">
                        @if (null($student->tasks[0]->student_file_path) && null($student->tasks[0]->student_score))
                            {{ trans('label.task_status', ['is' => "Belum"]) }}
                        @else
                            @if (null($student->tasks[0]->student_score))
                                {{ trans('label.task_status', ['is' => "Sudah"]) }}   
                            @else
                                {{ $student->tasks[0]->student_score }}
                            @endif
                        @endif
                    </h2>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="upload" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ trans('label.act.upload') }} {{ $student->tasks[0]->title }}</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close" title="Batal" data-toggle="tooltip">
                        <img src="{{ asset('web/images/icon-close.png') }}"/>
                    </button>
                </div>
                <form action="{{ route('student.task.upload', $student->tasks[0]->id) }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="custom-file">
                                <input name="file_path" 
                                       type="file" 
                                       class="custom-file-input {{ !$errors->first('file_path') ?: 'is-invalid' }}" 
                                       id="file_path" 
                                       title="Klik Untuk Memilih File"
                                       data-toggle="tooltip" 
                                       value="{{ old('file_path') ? old('file_path') : '' }}" />
                                <div class="invalid-feedback" style="display: inline;">
                                    {{ $errors->first('file_path') }}</div> 
                                <label class="custom-file-label" for="file_path">{{ trans('label.choose') }}</label>
                            </div>
                          </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success btn-round" type="submit">{{ trans('label.btn.up') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@push("javascript")
<script src="{{ asset('web/customs/js/ErrorsHandling.js') }}"></script>
<script>
    $(document).ready(function () {
        
        @if ($errors->has('file_path'))
             $('#upload').modal('show');
        @endif 

        $("#file_path").change(function() {
            var filename = $(this).val().split('\\')[2];
            
            $(this).parent().find('label').text(filename);
        });

        $("button.close").click(function() {
            $("#file_path").attr("value", null);

            setTimeout(function() {
                $("label[for = file_path]").text("Pilih...");
            }, 250)
        })
    });
</script>
@endpush