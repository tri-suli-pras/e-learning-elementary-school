@extends("dashboard.student.home")

@push("css")
@endpush

@section("content")
<div class="content-header">
	<h2 class="title">{{ trans('label.index_page_of', ['what' => "Tugas"]) }}</h2>
    <div class="right-content">
    	
    </div>
    @component('components.search_periode')@endcomponent
</div>

@if (session("undefined"))
    <div id="session" class="alert alert-warning alert-dismissible fade show" role="alert">
        <span class="lead">
            Oops!: {{ session("undefined") }}
        </span>
        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
@endif  

<div class="content-body">
	<div class="table-responsive-md">
		<table id="task" class="table table-custom">
			<thead>
				<th width="1%">{{ trans('label.for.numb') }}</th>
                <th width="25%">{{ trans('label.for.-L') }}</th>
                <th width="30%">{{ trans('label.for.-T') }}</th>
                <th >{{ trans('label.for.by') }}</th>
                <th width="1%"></th>
			</thead>

			<tbody>
                @empty ($datas->toArray())
                    <tr id="dlRow">
                        <td colspan="6" align="center"><b>{{ trans('label.empty') }}</b></td>
                    </tr>
                @else
    				@foreach ($datas as $task)
    				<tr id="dlRow">
                        <td align="center">{{ $loop->iteration }}</td>
                        <td>{{ $task->lessons->nama_matapelajaran }}</td>
                        <td align="center">{{ $task->title }}</td>
                        <td align="center">{{ $task->teachers->employees['nama_pegawai'] }}</td>
                        <td>
                            <a class="btn btn-outline-secondary btn-sm" href="{{ route('student.task.show', $task->id) }}">{{ trans('label.act.detail') }}</a>
                        </td>
                    </tr>
    				@endforeach
                @endempty
			</tbody>
		</table>
	</div>
</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/DataTable.js') }}" ></script>
<script src="{{ asset('web/customs/js/SessionMessages.js') }}"></script>
<script>
    @if (!empty($datas->toArray()))
        $(document).ready(function () {
            new TableController("#task");
        })
    @endif
</script>
@endpush