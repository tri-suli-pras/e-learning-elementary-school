<h1 class="logo">
	<a href="{{ route('student.home') }}">
		<img src="{{ asset('web/images/logo.png') }}"/>
		E-Learning
	</a>
</h1>
<div class="menu-toggle" style="z-index: 99999;"></div>

<ul class="menu">
     <li class="{{ Route::is('student.home') ? 'active' : '' }}">
        <a href="{{ route('student.home') }}">
            <div class="fa fa-dashboard"></div>
            <span>Dasbor</span>
        </a>
    </li>
    <li class="{{ Route::is('student.profile', auth()->guard('student')->user()->id) ? 'active' : '' }}">
        <a href="{{ route('student.profile', auth()->guard('student')->user()->id) }}">
            <div class="fa fa-user-circle"></div>
            <span>Profil</span>
        </a>
    </li>
    <li class="{{ explode('.', Route::currentRouteName())[1] === 'material' ? 'active' : '' }}">
        <a href="{{ route('student.material') }}">
            <div class="fa fa-book"></div>
            <span>Materi</span>
        </a>
    </li>

    <li class="{{ explode('.', Route::currentRouteName())[1] === 'task' ? 'active' : '' }}">
        <a href="{{ route('student.task') }}">
            <div class="fa fa-tasks"></div>
            <span>Tugas</span>
        </a>
    </li>

    <li class="{{ explode('.', Route::currentRouteName())[1] === 'quiz' ? 'active' : '' }}">
        <a href="{{ route('student.quiz') }}">
            <div class="fa fa-edit"></div>
            <span>Kuis</span>
        </a>
    </li>

	<li class="{{ explode('.', Route::currentRouteName())[1] === 'news' ? 'active' : '' }}">
		<a href="{{ route('student.news') }}">
      		<div class="fa fa-paper-plane"></div>
      		<span>Berita</span>
      	</a>
  	</li>
</ul>