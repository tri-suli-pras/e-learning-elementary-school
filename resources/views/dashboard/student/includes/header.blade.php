<h1 class="logo" style="margin-left: 16px;">
	<a href="{{ route('student.home') }}">
		MI Baiturrahman
	</a>
</h1>
<div class="dropdown">
	<div class="account" data-toggle="dropdown">
		<div class="image"><img src="images/profile-pic.jpg"/></div>
		<div class="content">
			<span class="name">
				{{ auth()->guard("student")->user()->nama_siswa }}
			</span>
			<span class="role">
				Siswa
			</span>
		</div>
	</div>

	<div class="dropdown-menu dropdown-menu-right">
		<a class="dropdown-item" href="{{ route('student.profile', auth()->guard('student')->user()->id) }}">Profil</a>
		<div class="dropdown-divider"></div>
		<a class="dropdown-item" href="{{ route('student.logout') }}">Logout</a>
	</div>
</div>