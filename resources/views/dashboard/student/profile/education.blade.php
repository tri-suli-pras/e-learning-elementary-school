<div id="education" class="tab-pane fade show" role="tabpanel" aria-labelledby="education-tab">
<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.school_mutation') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="ijazahterakhir_asal"
			   type="text"
			   class="form-control" 
			   value="{{ old('ijazahterakhir_asal') ? old('ijazahterakhir_asal') : $student->ijazahterakhir_asal }}"/>
		@else
			{{ $student->ijazahterakhir_asal }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.diploma') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="ijazahterakhir_no"
			   type="text"
			   class="form-control" 
			   value="{{ old('ijazahterakhir_no') ? old('ijazahterakhir_no') : $student->ijazahterakhir_no }}"/>
		@else
			{{ $student->ijazahterakhir_no }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.diploma_at') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input id="ijazahterakhir_tahun" 
			   name="ijazahterakhir_tahun"
			   type="number"
			   class="form-control {{ $errors->has('ijazahterakhir_tahun') ? 'is-invalid' : '' }}" 
			   title="{{ trans('label.must_number') }}"
			   data-toggle="tooltip"
			   value="{{ old('ijazahterakhir_tahun') ? old('ijazahterakhir_tahun') : $student->ijazahterakhir_tahun }}"/>
		@if ($errors->has('ijazahterakhir_tahun'))<div class="invalid-feedback">{{ trans('validation.must_a_number') }}</div>@endif
		@else
			{{ $student->ijazahterakhir_tahun }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.program_learn') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="program_pengajaran"
			   type="text"
			   class="form-control" 
			   value="{{ old('program_pengajaran') ? old('program_pengajaran') : $student->program_pengajaran }}"/>
		@else
			{{ $student->program_pengajaran }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.for.npsn') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input id="npsn" 
			   name="npsn"
			   type="number"
			   class="form-control {{ $errors->has('npsn') ? 'is-invalid' : '' }}" 
			   title="{{ trans('label.must_number') }}"
			   data-toggle="tooltip"
			   value="{{ old('npsn') ? old('npsn') : $student->npsn }}"/>
		@if ($errors->has('npsn'))<div class="invalid-feedback">{{ trans('validation.must_a_number') }}</div>@endif
		@else
			{{ $student->npsn }}
		@endif
	</dd>
</dl>
</div>