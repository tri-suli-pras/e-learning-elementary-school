<div id="biodata" class="tab-pane fade show active" role="tabpanel" aria-labelledby="biodata-tab">
<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.for.sex') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
			<select name="jenis_kelamin" class="select-control" title="Pilih Jenis Kelamin" disabled="true">
				<option value="L" {{ $student->jenis_kelamin === "L" ? "selected" : '' }}>Laki-Laki</option>
				<option value="P" {{ $student->jenis_kelamin === "P" ? "selected" : '' }}>Perempuan</option>
			</select>
		@else
			{{ is_equal($student->jenis_kelamin, "L") ? trans('label.male') : trans('label.female') }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.for.POB') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="tempat_lahir" 
			   type="text" class="form-control"
			   readonly 
			   value="{{ old('tempat_lahir') ? old('tempat_lahir') : $student->tempat_lahir }}">
		@else
			{{ $student->tempat_lahir }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.for.BOD') }}</h4></dt>
	<div class="col-6">
		@if (! isset($months))
		<div class="date-control">
			<input name="tanggal_lahir"
				   type="text" 
				   class="form-control" 
				   value="{{ old('tanggal_lahir') ? old('tanggal_lahir') : $student->tanggal_lahir }}"
				   readonly>
			<label class="fa fa-calendar" for="startDate"></label>
		</div>
		@else
			{{ $student->tanggal_lahir }}
		@endif
	</div>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.for.religion') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="agama"
			   type="text"
			   class="form-control" 
			   readonly 
			   value="{{ old('agama') ? old('agama') : $student->agama }}">
		@else
			{{ $student->agama }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.body.-T') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input id="tinggi_badan" 
		      name="tinggi_badan"
			  type="number"
			  class="form-control {{ $errors->has('tinggi_badan') ? 'is-invalid' : '' }}" 
			  title="{{ trans('label.must_number') }}"
			  data-toggle="tooltip" 
			  value="{{ old('tinggi_badan') ? old('tinggi_badan') : $student->tinggi_badan }}"/>
			@if ($errors->has('tinggi_badan'))<div class="invalid-feedback">{{ $errors->first('tinggi_badan') }}</div>@endif
		@else
			{{ $student->tinggi_badan }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.body.-W') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input id="berat_badan"
		       name="berat_badan"
			   type="number"
			   title="{{ trans('label.must_number') }}"
			   data-toggle="tooltip"
			   class="form-control {{ $errors->has('berat_badan') ? 'is-invalid' : '' }}" 
			   value="{{ old('berat_badan') ? old('berat_badan') : $student->berat_badan }}"/>
		@if ($errors->has('berat_badan'))<div class="invalid-feedback">{{ $errors->first('berat_badan') }}</div>@endif
		@else
			{{ $student->berat_badan }}			
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.for.child_order') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="anak_ke"
			   type="text"
			   class="form-control" 
			   readonly 
			   value="{{ old('anak_ke') ? old('anak_ke') : $student->anak_ke }}"/>
		@else
			{{ "" }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.for.special_needs') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input name="berkebutuhan_khusus"
			   type="text"
			   class="form-control"
			   readonly 
			   value="{{ old('berkebutuhan_khusus') ? old('berkebutuhan_khusus') : $student->berkebutuhan_khusus }}"/>
		@else
			{{ $student->berkebutuhan_khusus }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.for.phone') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input id="no_tlp"
		       name="no_tlp"
			   type="number"
			   title="{{ trans('label.must_number') }}"
			   data-toggle="tooltip"
			   class="form-control {{ $errors->has('no_tlp') ? 'is-invalid' : '' }}" 
			   value="{{ old('no_tlp') ? old('no_tlp') : $student->no_tlp }}"/>
		@if ($errors->has('no_tlp'))<div class="invalid-feedback">{{ trans('validation.phone_number') }}</div>@endif
		@else
			{{ $student->no_tlp }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.for.hp') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input id="no_hp"
		       name="no_hp"
			   type="number"
			   title="{{ trans('label.must_number') }}"
			   data-toggle="tooltip"
			   class="form-control {{ $errors->has('no_hp') ? 'is-invalid' : '' }}" 
			   value="{{ old('no_hp') ? old('no_hp') : $student->no_hp }}"/>
		@if ($errors->has('no_hp'))<div class="invalid-feedback">{{ trans('validation.phone_number') }}</div>@endif
		@else
			{{ $student->no_hp }}
		@endif
	</dd>
</dl>

<dl class="row">
	<dt class="col-2"><h4>{{ trans('label.for.@') }}</h4></dt>
	<dd class="col-6">
		@if (! isset($months))
		<input id="email"
			   name="email"
			   type="text"
			   class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" 
			   value="{{ old('email') ? old('email') : $student->email }}"/>
		@if ($errors->has('email'))<div class="invalid-feedback">{{ trans('validation.custom.email') }}</div>@endif
		@else
			{{ $student->email }}
		@endif
	</dd>
</dl>
</div>