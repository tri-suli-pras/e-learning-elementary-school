@extends("dashboard.student.home")

@push("css")
@endpush

@section("content")
<div class="content-header">
    <div>
        <a href="{{ route('student.material') }}"> 
            <div class="fa fa-angle-double-left mr-2"></div>
            {{ trans('label.index_page_of', ['what' => "Materi"]) }}
        </a>
    </div>

    <h2 class="title">
	   	{!! trans('label.detail', ['for' => "Materi", 'what' => $material->lessons->nama_matapelajaran]) !!}
    </h2>
</div>
<div class="content-body">
	<div class="row">
		<div class="col-md-8">
			<dl id="dlRow" class="row">
				<dt class="col-4"><h4>{{ trans('label.for.-T') }}</h4></dt>
				<dd class="col-8">{{ $material->title }}</dd>

				<dt class="col-4"><h4>{{ trans('label.for.-C') }}</h4></dt>
				<dd class="col-8">{{ "{$material->class_room} {$material->pararel}" }}</dd>

				<dt class="col-4"><h4>{{ trans('label.for.-D') }}</h4></dt>
				<dd class="col-8">{{ null($material->description) ? trans('label.empty') : $material->description }}</dd>

				<dt class="col-4"><h4>{{ trans('label.for.-H') }}</h4></dt>
				<dd class="col-8">{{ \Carbon\Carbon::parse($material->created_at)->diffForHumans() }}</dd>

				<dt class="col-4"><h4>{{ trans('label.for.-O') }}</h4></dt>
				<dd class="col-8">
					@if (explode('.', $material->file_path)[1] === "pdf")
						<a class="badge badge-info" href="{{ route('student.material.read', $material->id) }}" target="_blank">{{ trans('label.act.reading') }}</a>
					@endif
					<a class="badge badge-success" href="{{ asset($material->file_path) }}" download="{{ $material->title . '.' .explode('.', $material->file_path)[1] }}">{{ trans('label.act.download') }}</a>
				</dd>
			</dl>
		
			<hr />
		</div>

		<div class="col-md-4">
			@if (explode('.', $material->file_path)[1] === "pdf")
				<object id="pdf" data="{{ asset($material->file_path) }}" width="250px" height="250px" type="application/pdf" frameborder="5"></object>
			@endif
			<hr/>
		</div>
	</div>
</div>
@endsection

@push("javascript")
<script>
</script>
@endpush