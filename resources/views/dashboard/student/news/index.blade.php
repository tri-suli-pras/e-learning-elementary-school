@extends("dashboard.student.home")

@push('css')
<link rel="stylesheet" href="{{ asset('web/customs/css/option-hover.css') }}">
@endpush

@section("content")
<div class="content-header">
	<h2 class="title">{{ trans('label.index_page_of', ['what' => "Berita"]) }}</h2>
    <div class="right-content"></div>
</div> 
<div class="content-body">
	<div class="table-responsive-md">
		<table id="news" class="table table-custom">
            <thead>
                <th width="1%">{{ trans('label.for.numb') }}</th>
                <th width="20%">{{ trans('label.for.-T') }}</th>
                <th>{{ trans('label.category') }}</th>
                <th>{{ trans('label.for.by') }}</th>
                <th>{{ trans('label.for.-H') }}</th>
                <th width="1%"></th>
            </thead>

            <tbody>
                @empty ($datas->toArray())
                    <tr id="dlRow">
                        <td colspan="7" align="center"><b>{{ trans('label.empty') }}</b></td>
                    </tr>
                @else
                    @php
                        $i = 1;
                    @endphp
                    @foreach ($datas as $news)
                    @continue(empty($news->students->toArray()) && empty($news->classnews->toArray()))
                    <tr id="dlrow">
                        <td align="center">{{ $i }}</td>
                        <td>{{ $news->title }}</td>
                        <td align="center">
                            @if ($news->receiver === "Management")
                                {{ trans('label.for.-M') }}
                            @elseif ($news->receiver === "Student")
                                {{ trans('label.for.-S') }}
                            @elseif ($news->receiver === "Class")
                                {{ trans('label.for.-C') }}
                            @else
                                {{ trans('label.common') }}
                            @endif
                        </td>
                        <td align="center">{{ $news->teachers->employees['nama_pegawai'] }}</td>
                        <td align="center">{{ $news->created_at->diffForHumans() }}</td>
                        <td><a class="btn btn-success btn-sm" href="{{ route('student.news.detail', $news->id) }}">Detail</a></td>
                    </tr>
                    @php
                        $i++
                    @endphp
                    @endforeach
                @endempty
            </tbody>
		</table>
	</div>
</div>
@endsection


@push("javascript")
<script src="{{ asset('web/customs/js/SessionMessages.js') }}" ></script>
<script src="{{ asset('web/customs/js/DataTable.js') }}" ></script>
<script>
    $(document).ready(function () {
        @if (! empty($datas->toArray()))
            new TableController("#news");
        @endif
    });
</script>
@endpush