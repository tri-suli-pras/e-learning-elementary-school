@extends("dashboard.student.home")

@push("css")
<link rel="stylesheet" href="{{ asset('web/customs/css/count-down.css') }}">
@endpush

@section("content")
<div class="row justify-content-center">
	<div class="col-md-12">
		<div class="content-header mb-0">
			<div class="right-content">
		    	<a id="send" class="btn btn-primary pull-right btn-add" href="#alert" data-toggle="modal">
		    		<span>Kirim</span>
		    	</a>
		    </div>
			<div class="table-responsive-md">
				<table class="table table-custom">
					<tbody>
						<table class="col-md-6" width="75%">
							<tr>
								<td><b>Nama</b></td>
								<td><b>:</b></td>
								<td>{{ student()->getName() }}</td>
							</tr>

							<tr>
								<td><b>NISN</b></td>
								<td><b>:</b></td>
								<td>{{ student()->getNisn() }}</td>
							</tr>

							<tr>
								<td><b>Waktu kuis</b></td>
								<td><b>:</b></td>
								<td>{{ $quiz->count_down }} Menit</td>
							</tr>

							@if (! empty($questions['multiple']))
							<tr>
								<td><b>Pilihan Ganda</b></td>
								<td><b>:</b></td>
								<td>{{ count($questions['multiple']) }} Soal</td>
							</tr>
							@endif
							@if (! empty($questions['essay']))
							<tr>
								<td><b>Essay</b></td>
								<td><b>:</b></td>
								<td>{{ count($questions['essay']) }} Soal</td>
							</tr>
							@endif
						</table>
					</tbody>
				</table>
			</div>
		</div>
		<div class="count-down ">00:00 menit</div>
		<div class="content-body">
			<div class="row">
				<div class="col-md-12">
					<form action="{{ route('student.quiz.store', $quiz->id) }}" method="POST">
						{{ csrf_field() }}
							@if (! empty($questions['multiple']))
								@include(dashboard("quiz.questions", "multiple"))
							@endif
							@if (! empty($questions['essay']))
								@include(dashboard("quiz.questions", "essay"))
							@endif
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section("modal")
<div class="modal fade" id="alert" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Simpan Kuis</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('web/images/icon-close.png') }}"/>
                </button>
            </div>
            <div class="modal-body">
                <p>Apa Anda Sudah Yakin Dengan Jawaban Anda ?</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">
		            Tidak
		        </button>
		        <button id="yes" class="btn btn-primary" type="button">YA</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="timeout" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Waktu Habis</h5>
                <button disabled class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('web/images/icon-close.png') }}"/>
                </button>
            </div>
            <div class="modal-body">
                {{-- <p>Tekan tombol OK untuk menyimpan kuis anda.</p> --}}
            </div>
            <div class="modal-footer">
		        {{-- <button id="ok" class="btn btn-primary" type="button">OK</button> --}}
            </div>
        </div>
    </div>
</div>
@endsection

@push("javascript")
<script>
	$(document).ready(function () {
		$("#yes").click(function() {
			$('form').submit();
		});
		
		$("input").change(function () {
			let Iparent = $(this).parent();
			let mtpNumb = Iparent.parent().parent().parent();

			console.log(mtpNumb.attr('id'));
		});
	});

	$(function () {
		let quizInterval = setInterval(() => {
			var oneDay = 24 * 60 * 60 * 1000;
			var firstDate = new Date("{{ \Carbon\Carbon::parse($questions['time'])->format('d M Y H:i') }}");
			var secondDate = new Date();
			var days = (firstDate.getTime() - secondDate.getTime()) / (oneDay);
			var hrs = (days - Math.floor(days)) * 24;
			var min = (hrs - Math.floor(hrs)) * 60;

			var sec = Math.floor((min - Math.floor(min))* 60);
			let time = `${Math.floor(hrs)} Jam` + ' ' + `${(Math.floor(min))} Menit` + ' ' + `${sec} Detik`;
			
			console.log(time);
			$(".count-down").text(time);

			if (firstDate < secondDate) {
				
				$("#timeout").modal("show");
				
				setTimeout(function () {
					$("form").submit();
				}, 2500);

				$(".count-down").text("Waktu Habis");

				clearInterval(quizInterval);
			}
		}, 1000)	
	});
</script>
@endpush