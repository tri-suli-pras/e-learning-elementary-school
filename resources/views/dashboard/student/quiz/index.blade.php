@extends("dashboard.student.home")

@push("css")

@endpush

@section("content")
<div class="content-header">
	<h2 class="title">{{ trans('label.index_page_of', ['what' => "Kuis"]) }}</h2>
    <div class="right-content">
        @if (session("success"))
        <div id="session" class="alert alert-success alert-dismissible fade show" role="alert">
            <span class="lead">{{ trans('confirmation.success') }}</span>{{ session("success") }}
            <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        @endif 
    </div>
    @component('components.search_periode')@endcomponent
</div>

<div class="content-body">
	<div class="table-responsive-md">
		<table id="quiz" class="table table-custom">
			<thead>
				<th width="1%">{{ trans('label.for.numb') }}</th>
                <th width="25%">{{ trans('label.for.-L') }}</th>
                <th width="15%">{{ trans('label.for.-T') }}</th>
                <th>{{ trans('label.for.by') }}</th>
                <th>{{ trans('label.for.-s') }}</th>
                <th width="1%"></th>
			</thead>
			<tbody>
                @empty ($datas->toArray())
                    <tr id="dlRow">
                        <td colspan="7" align="center">{{ trans('label.empty') }}</td>
                    </tr>
                @else
                    @foreach ($datas as $quiz)
                        <tr id="dlRow{{ $loop->iteration }}">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $quiz->lessons->nama_matapelajaran }}</td>
                            <td>{{ $quiz->title }}</td>
                            <td>{{ $quiz->teachers->employees['nama_pegawai'] }}</td>
                            <td id="status{{ $loop->iteration }}" align="center">
                                @if ($quiz->status === "true")
                                <span id="{{ $loop->iteration }}" class="badge badge-success status">{{ trans('label.quiz_status', ['is' => "Sudah"]) }}</span>
                                @else
                                    <span id="{{ $loop->iteration }}" class="badge badge-info status">{{ trans('label.quiz_status', ['is' => "Belum"]) }}</span>
                                @endif
                                <span id="start{{ $loop->iteration }}" hidden>{{ \Carbon\Carbon::parse($quiz->starting_time)->toDateTimeString() }}</span>
                                <span id="end{{ $loop->iteration }}" hidden>{{ \Carbon\Carbon::parse($quiz->starting_time)->addMinutes($quiz->count_down)->toDateTimeString() }}</span>
                            </td>
                            <td>
                                <a class="btn btn-outline-secondary btn-sm" href="{{ route('student.quiz.detail', $quiz->id) }}">{{ trans('label.act.detail') }}</a>
                            </td>
                        </tr>
                    @endforeach
                @endempty
			</tbody>
		</table>
	</div>
</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/SessionMessages.js') }}"></script>
<script src="{{ asset('web/customs/js/DataTable.js') }}"></script>
<script>
    $(document).ready(function () {
        @if (! empty($datas->toArray()))
        
        new TableController("#quiz");
        
        let trIndex = {{ count($datas) }};

        for (let i = 1; i <= trIndex; i++) {
            
            let tdStatus = $(`tr#dlRow${i}`).find(`td#status${i}`);
            let spanStart = tdStatus.find(`#start${i}`),
                spanEnd = tdStatus.find(`#end${i}`);

            let quiz = {
                start: new Date(spanStart.text()),
                end: new Date(spanEnd.text())
            };
                
            let begin = setInterval(function() {
                
                let date = new Date();

                if (date.getTime() >= quiz.start.getTime() && date.getTime() <= quiz.end.getTime()) {

                    tdStatus.find(`span#${i}`).text("{{ trans('label.quiz_status', ['is' => "Sedang"]) }}");

                    if (date.getSeconds() % 2 === 0) {
                        tdStatus.find(`span#${i}`).css("background-color", "#ff3f34");
                    }
                    else
                        tdStatus.find(`span#${i}`).css("background-color", "#ffa801");
                }
                else if (date.getTime() > quiz.end.getTime()) {
                    
                    tdStatus.find(`span#${i}`).text("{{ trans('label.quiz_status', ['is' => "Sudah"]) }}").css("background-color", "#18a566");

                    setTimeout(function() {
                        
                        clearInterval(begin)
                    }, 0)
                }
            }, 1000);
        }
        @endif
    });
</script>
@endpush