@extends("dashboard.management.home")

@push("css")
<style>
    #img-profil {
        width: 250px;
        height: 250px;
        border-radius: 10px;
        border: 1px solid black;
        margin: 0 49px;
    }

    #img-input {
        display: block;
        position: absolute;
        top: 288px;
        left: 51px;
    }

    .custom-file-input:lang(en)~.custom-file-label::after {
        content: "Pilih File";
        background-color: #18a566;
        color: #fff;
    }
</style>
@endpush

@section("content")
<div class="content-header">
    <div>
        <a href="{{ route('management.employee.show', auth()->user()->pegawai_id) }}"> 
            <div class="fa fa-angle-double-left mr-2"></div>
            {{ trans('label.profile.-P') }}
        </a>
    </div>
    <h2 class="title">{{ $employee->nama_pegawai }}</h2>
    @if ($errors->any())
        <div class="right-content">
            <span>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <span class="lead">{{ trans('confirmation.error') }}</span>
                    {{ trans('confirmation.failure') }}
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </span>
        </div>
    @endif
</div>

<div class="content-body">
    <div class="row">
        <div class="col-md-12">
            @include("forms.employee")
        </div>
    </div>
</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/InputValidation.js') }}"></script>
<script>
    $(function () {
       var input = new InputValidation(
            'nip',
            'nama_pegawai',
            'alamat',
            'no_telp',
            'foto_pegawai',
            'CPNS_TMT',
            'PNS_TMT',
            'tmt_guru_tanggal',
            'tmt_guru_bulan',
            'tmt_guru_tahun',
            'sertifikasi_tahun',
        );

        input.validated();
    });
</script>
@endpush