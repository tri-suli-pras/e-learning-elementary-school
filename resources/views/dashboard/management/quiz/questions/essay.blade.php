<div id="esy" class="{{ !Route::is('quiz.show', $quiz->id) ? 'show' : '' }} mt-3">
	<nav aria-label="breadcrumb">
        <ol class="breadcrumb">Essay.</ol>
    </nav>
	@if (! empty($questions['essay']))
		@foreach ($questions['essay'] as $i => $question)
			@continue(null($question['no']))
			<div class="form-group">
				<div class="row">
					<div class="col-md-7">
						<label for="question">
							{{ $loop->iteration }}. {{ $question['exercise'] }}
						</label>
				
						<textarea disabled class="form-control" id="" rows="5">@if (Route::is("quiz.essay") && $studentAnswers){{ $studentAnswers['essay'][$i]['answers'] }}@endif</textarea>
						@if (Route::is("quiz.essay"))
						    <input name="essay_scores[no{{ $loop->iteration }}] {{ !$errors->first("essay_scores.no{$loop->iteration}") ?: 'is-invalid' }}" 
						    	  type="number" 
						    	  class="form-control" 
						    	  placeholder="Masukan Nilai" 
						    	  value="{{ old("essay_scores.no{$loop->iteration}") ? old("essay_scores.no{$loop->iteration}") : $studentAnswers['essay'][$i]['score']  }}" />
						    <div class="invalid-feedback" style="display: inline;">{{ $errors->first("essay_scores.no{$loop->iteration}") }}</div> 
						    @if (!!$quiz->is_updated)
							    <dl id="dlRow" class="row mt-3">
							    	<dt class="col-12" style="margin-bottom: 0;"><h3><strong>Koreksi Sistem</strong></h3></dt>
	                				<dt class="col-9">
	                					Kata Kunci&nbsp;:
	                					<h5>
	                						<p>{{ $studentAnswers['essay'][$i]['answers_key'] }}</p>
	                					</h5>
	                				</dt>
	                				<dd class="col-3">
	                					Nilai&nbsp;:&nbsp;
	                					<h5><p>{{ $studentAnswers['essay'][$i]['score'] }}</p></h5>
	                				</dd>
							    </dl>
						    @endif
						@endif
					</div>

					<div class="col-md-5">
						<img src="data:image/jpeg;base64,{{ $question['img'] }}" alt="" style="width: 100%; height: auto;">
					</div>
				</div>
			</div>
			<hr />
		@endforeach
	@endif
</div>