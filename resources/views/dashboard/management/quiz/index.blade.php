@extends("dashboard.management.home")

@push("css")
<link rel="stylesheet" href="{{ asset('web/customs/css/option-hover.css') }}">
<style>
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
@endpush

@section("content")
<div class="content-header">
	<h2 class="title">{{ trans('label.index_page_of', ['what' => "Kuis"]) }}</h2>
    <div class="right-content">
    	<a class="btn btn-primary pull-right btn-add" href="{{ route('quiz.create') }}">
    		<span>{{ trans('label.act.add', ['what' => "Kuis"]) }}</span>
    	</a>
    </div>
    @component('components.search_periode')@endcomponent
</div>
@if (session("store") || session("delete") || session('update'))
    <div id="session" class="alert alert-success alert-dismissible fade show" role="alert">
        <span class="lead">{{ trans('confirmation.success') }}</span>
        {{ session("store") ? session("store") : (session("delete") ? session("delete") : session("update")) }}
        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
@endif
<div class="content-body">
	<div class="table-responsive-md">
		<table id="quiz" class="table table-custom">
            <thead>
                <th width="1%">{{ trans('label.for.#') }}</th>
                <th width="20%">{{ trans('label.for.-L') }}</th>
                <th width="15%">{{ trans('label.for.-T') }}</th>
                <th width="5%">{{ trans('label.for.-C') }}</th>
                @role('admin')<th>{{ trans('label.for.by') }}</th>@endrole
                <th width="10%">{{ trans('label.for.-s') }}</th>
                <th width="1%"></th>
            </thead>
            <tbody id="tbody">
                @empty ($datas->toArray())
                    <tr id="dlRow">
                        <td colspan="7" align="center"><b>{{ trans('label.empty') }}</b></td>
                    </tr>
                @else
                    @foreach ($datas as $quiz)
                    <tr id="dlRow{{ $loop->iteration }}">
                        <td align="center">{{ $loop->iteration }}</td>
                        <td class="sempak">{{ $quiz->lessons->nama_matapelajaran }}</td>
                        <td>{{ $quiz->title }}</td>
                        <td align="center">{{ "{$quiz->class_room} {$quiz->pararel}" }}</td>
                        @role('admin')<td align="center">{{ $quiz->teachers->employees['nama_pegawai'] }}</td>@endrole
                        <td id="status{{ $loop->iteration }}" align="center">
                            @if ($quiz->status === "true")
                                <span id="{{ $loop->iteration }}" class="badge badge-success status">{{ trans('label.quiz_status', ['is' => "Sudah"]) }}</span>
                            @else
                                <span id="{{ $loop->iteration }}" class="badge badge-info status">{{ trans('label.quiz_status', ['is' => "Belum"]) }}</span>
                            @endif
                            <span id="start{{ $loop->iteration }}" hidden>{{ \Carbon\Carbon::parse($quiz->starting_time)->toDateTimeString() }}</span>
                            <span id="end{{ $loop->iteration }}" hidden>{{ \Carbon\Carbon::parse($quiz->starting_time)->addMinutes($quiz->count_down)->toDateTimeString() }}</span>
                        </td>
                        <td id="nav{{ $loop->iteration }}">
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary btn-sm" type="button" data-toggle="dropdown">{{ trans('label.for.-O') }}</button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item dtl" href="{{ route('quiz.detail', $quiz->id) }}">{{ trans('label.act.detail') }}</a>
                                    <a class="dropdown-item look" href="{{ route('quiz.show', $quiz->id) }}">{{ trans('label.act.exercise') }}</a>
                                    @if ( is_equal($quiz->user_id, auth()->user()->id) && $quiz->status === 'false')
                                        <a class="dropdown-item edit" href="{{ route('quiz.edit', $quiz->id) }}">{{ trans('label.act.edit') }}</a>
                                        <a class="dropdown-item del" href="{{ '#delete'.$quiz->id }}" data-toggle="modal">{{ trans('label.act.delete') }}</a>
                                    @endif
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                @endempty
            </tbody>
		</table>
	</div>
</div>
@endsection

@section("modal")
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/SessionMessages.js') }}"></script>
<script src="{{ asset('web/customs/js/ErrorsHandling.js') }}"></script>
<script src="{{ asset('web/customs/js/DataTable.js') }}"></script>
<script>
    $(document).ready(function () {
        
        @if (! empty($datas->toArray()))

        new TableController("#quiz");

        let trIndex = {{ count($datas) }};

        for (let i = 1; i <= trIndex; i++) {
            
            let tdStatus = $(`tr#dlRow${i}`).find(`td#status${i}`);
            let spanStart = tdStatus.find(`#start${i}`),
                spanEnd = tdStatus.find(`#end${i}`);

            let quiz = {
                start: new Date(spanStart.text()),
                end: new Date(spanEnd.text())
            };

            function hideEditAndDeleteButton () {
                
                $(`tr#dlRow${i}`).find(`td#nav${i}`).find('a').eq(2).hide();
                $(`tr#dlRow${i}`).find(`td#nav${i}`).find('a').eq(3).hide();
            }
                
            let begin = setInterval(function() {
                
                let date = new Date();

                if (date.getTime() >= quiz.start.getTime() && date.getTime() <= quiz.end.getTime()) {

                    tdStatus.find(`span#${i}`).text('{{ trans('label.quiz_status', ['is' => "Sedang"]) }}');

                    if (date.getSeconds() % 2 === 0) {
                        tdStatus.find(`span#${i}`).css("background-color", "#ff3f34");
                        hideEditAndDeleteButton();
                    }
                    else {

                        tdStatus.find(`span#${i}`).css("background-color", "#ffa801");
                        hideEditAndDeleteButton();
                    }
                }
                else if (date.getTime() > quiz.end.getTime()) {
                    
                    tdStatus.find(`span#${i}`).text('{{ trans('label.quiz_status', ['is' => "Sudah"]) }}').css("background-color", "#18a566");

                    setTimeout(function() {
                        
                        clearInterval(begin)
                    }, 0)
                }
            }, 1000);

        }
        @endif
    });
</script>
@endpush