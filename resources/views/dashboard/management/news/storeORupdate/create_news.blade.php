@extends("dashboard.management.home")

@section("content")
<div class="row justify-content-center">
	<div class="col-md-12">
		<div class="content-header">
	        <h2 class="title">
	        	{{ trans('label.for.-T') }} {{ ": {$datas['title']}" }}
	        </h2>
	    </div>

	    <div class="content-body">
	    	@include("forms.news.write")
	    </div>
	</div>
</div>
@endsection

@push("javascript")
<script>
	$(document).ready(function () {
		
		$("#editor").summernote({
			
			height	: 300,
			focus	: true,
			toolbar : [
				['style', ['bold', 'italic', 'underline', 'clear']],
				['fontsize', ['fontsize']],
				['font', ['superscript', 'subscript']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph', 'height', 'style']],
				['fontname'], ['undo'], ['redo'],
				['insert', ['hr', 'picture', 'table', 'link']],
			],
			placeholder: "Isi Pengumuman...",
			dialogsInBody: true,
			callbacks: {
				onEnter: () => {
					$('p').css("margin", 0);
				}
			}
		});
	});
</script>
@endpush