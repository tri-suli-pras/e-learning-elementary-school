@extends("dashboard.management.home")

@section("content")
<div class="row justify-content-center">
	<div class="col-md-6">
		<div class="content-header">
	        <div>
	        	<a href="{{ route('news') }}"> 
	            	<div class="fa fa-angle-double-left mr-2"></div>
	            	{{ trans('label.index_page_of', ['what' => "Berita"]) }}
	            </a>

	            @isset ($news)
	                &nbsp;||&nbsp;
		            <a href="{{ route('news.detail', $news->id) }} }}"> 
		            	{{ trans('label.act.add', ['what' => "Berita"]) }}
		            	<div class="fa fa-angle-double-right mr-2"></div>
		            </a>
	            @endisset
	        </div>
	        <h2 class="title">
	        	{{ isset($news) ? 
	        		trans('label.act.add', ['what' => "Berita"]) : 
	        		trans('label.act.edit_is', ['what' => "Berita"]) 
	        	}}
	        </h2>
	    </div>

	    <div class="content-body">
	    	@include("forms.news.category")
	    </div>
	</div>
</div>
@endsection

@push("javascript")
<script>
	$(document).ready(function() {
		
		var CLASSROOM	= "classroom",
			EMPLOYEE	= "management",
			COMMON		= "common",
			STUDENT		= "student";
		
		var Select = {
			receiver: $("select[name=receiver]"),
			student: $("select#student")
		};
		var InputTitle = $("input[name=title]");
		var ButtonOK = {
			status: false,
			message: "{{ trans('label.form_must_filled') }}",
			setStatus: function ( status ) {
				
				btn = $("button[type=submit]");
				this.status = status;
				var on = true;

				if ( this.status === on) {
					btn.attr({ disabled: false, title: '' });
				}
				else 
					btn.attr({ 
						disabled: true,
						title: this.message,
						'data-toggle': 'tooltip',
						'data-original-title': this.message
					});

			}
		};
		var Div = {
			cls: {
				comp: $("div#cls"),
				status: false,
			},
			std: {
				comp: $("div#std"),
				status: false
			},
			setStatus: function ( id, open ) {
					
				if ( id === "cls" ) {

					this.cls.status = open;
					this.std.status = !open;
				}
				else if ( id === 'std' ) {
					
					this.std.status = open;
					this.cls.status = !open;
				}

				return this;
			},
			isOpen: function ( status ) {

				return status === true;
			},
			show: function ( closure = null ) {

				if ( this.isOpen(this.cls.status) ) {
					this.cls.comp.show(500, closure);
					this.std.comp.hide(500);
				}
				else if ( this.isOpen(this.std.status) ) {
					this.std.comp.show(500, closure);
					this.cls.comp.hide(500);
				}
			}
		};
		var nextPage = function ( title, category ) {
			
			var origin = window.location.origin;

			@if (isset($news))
				var id = "{{ $news->id }}";
				if (category) {
					return origin + `/e-learning/berita/edit-berita/${id}/${title}/kategori/${category}`;
				}
				else 
					return origin + `/e-learning/berita/edit-berita/${id}/${title}/kategori/{{ $news->receiver }}`;
			@else
				return origin + `/e-learning/berita/tambah-berita/${title}/kategori/${category}`;
			@endif
		};
		var actions = function () {
			ButtonOK.setStatus(false);

			InputTitle.keyup(function (event) {
				
				var title = $(this).val(),
					category = langId(Select.receiver.val());

				if ( title.length > 0 && category !== undefined ) {
					if (category == "kelas") {
						if ($('input[type = checkbox]').filter(':checked').length > 0) {
							$("form").attr({
								action: nextPage(title, category)
							});
							ButtonOK.setStatus(true);
						}
					}
					else if (category == "siswa") {
						if ($("#display").find("span").length > 0) {
							$("form").attr({
								action: nextPage(title, category)
							});
							ButtonOK.setStatus(true);
						}
					}
					else {
						$("form").attr({
								action: nextPage(title, category)
							});
							ButtonOK.setStatus(true);
					}
				}
				else {
					ButtonOK.message = "Lengkapi form anda";
					ButtonOK.setStatus(false);
					return title;
				}
			}).parent()
			  .next()
			  .find("select[name=receiver]").change(function(event) {
			  	var title = InputTitle.val(),
			  		thisVal = $(this).val(),
			  		category = langId(thisVal);
			
				switch ( thisVal ) {
					case CLASSROOM:
						Div.setStatus("cls", true).show(function () {
							if ($(this).find('input[type = checkbox]').filter(":checked").length < 1) ButtonOK.setStatus(false);

								var inputCheckbox = $(this).find('input[type = checkbox]');
								inputCheckbox.each(function () {
									var thisInput = $(this);
										thisInput.click(function () {
											if (inputCheckbox.filter(":checked").length > 0 && InputTitle.val().length > 0) { 
													$("form").attr({
														action: nextPage(InputTitle.val(), category)
													});
		                                    		ButtonOK.setStatus(true);
		                                    }
		                                    else if (inputCheckbox.filter(":checked").length < 1) {
		                                    	$("form").attr({
													action: null
												});
		                                        ButtonOK.setStatus(false);
		                                    }
										})
								});
						});

						return;
					case STUDENT:
						Div.setStatus("std", true).show(function () {
							Select.student.trigger("change");
						});
						return;
					default:
						Div.cls.comp.hide(500, function () {

						});
						Div.std.comp.hide(500);

						if (title.length > 0 && category !== undefined) {
							$("form").attr({
								action: nextPage(title, category)
							});
							ButtonOK.setStatus(true);
						}
						else {
							ButtonOK.setStatus(false)
							return;
						}
						break;
				}
			})
			

			Select.student.change(function () {
				var $this = $(this),
					span = "";
				
				$this.find("option:selected").each(function (element, index) {
					
					var id = $(this).val(),
                    	nisn = $(this).attr('data-subtext');

                    	span += `<span class="badge badge-success">${nisn}
                           	<input name="student_id[]" type="text" hidden value="${id}" />
                        </span>`;
				})
				
				$("#display").find('span').remove();
                $("#display").append(span);

                if ($("#display").find("span").length > 0 && InputTitle.val().length > 0) {
                	$("form").attr({
						action: nextPage(InputTitle.val(), langId(Select.receiver.val()))
					});
                	ButtonOK.setStatus(true);
                }
                else {
                	$("form").attr({ action: null });
                	ButtonOK.setStatus(false);
                }
			});

			@isset ($news)
				Select.receiver.trigger("change");
				Select.student.trigger("change");
				InputTitle.trigger("keyup");
			    @if (! empty($news->classnews->toArray()))
			    	//Div.setStatus("cls", true).show();

			    	@foreach ($news->classnews as $class)
			    		$(`#class{{ $class->class_room }}{{ $class->pararel }}`)
			    			.attr('checked', "true");
			    	@endforeach
			    @endif
			@endisset
		}; actions();
		
		function langId ( word ) {
			if ( word === COMMON ) {
				return 'umum';
			}
			else if ( word === EMPLOYEE ) {
				return 'manajemen';
			}
			else if ( word === CLASSROOM ) {
				return 'kelas';
			}
			else if ( word === STUDENT ) {
				return 'siswa'
			}
		}
	});
</script>
@endpush