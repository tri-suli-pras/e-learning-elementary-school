<h1 class="logo">
	<a href="{{ route('management.home') }}">
		<img src="{{ asset('web/images/logo.png') }}"/>
		E-Learning
	</a>
</h1>
<div class="menu-toggle" style="z-index: 99999;"></div>

<ul class="menu">
    <li class="{{ Route::is('management.home') ? 'active' : '' }} ">
        <a href="{{ route('management.home') }}">
            <div class="fa fa-dashboard"></div>
            <span>Dasbor</span>
        </a>
    </li>

    <li class="{{ Route::is('management.employee.show', auth()->user()->pegawai_id) ? 'active' : '' }}">
        <a href="{{ route('management.employee.show', auth()->user()->pegawai_id) }}">
            <div class="fa fa-user-circle"></div>
            <span>Profil</span>
        </a>
    </li>

    <li class="{{ explode('.', Route::currentRouteName())[0] === "material" ? 'active' : '' }}">
        <a href="{{ route('material') }}">
            <div class="fa fa-book"></div>
            <span>Materi</span>
        </a>
    </li>

    <li class="{{ explode('.', Route::currentRouteName())[0] === "task" ? 'active' : '' }}">
        <a href="{{ route('task') }}">
            <div class="fa fa-tasks"></div>
            <span>Tugas</span>
        </a>
    </li>

    <li class="{{ explode('.', Route::currentRouteName())[0] === "quiz" ? 'active' : '' }}">
        <a href="{{ route('quiz') }}">
            <div class="fa fa-edit"></div>
            <span>Kuis</span>
        </a>
    </li>

	<li class="{{ explode('.', Route::currentRouteName())[0] === "news" ? 'active' : '' }}">
		<a href="{{ route('news') }}">
      		<div class="fa fa-paper-plane"></div>
      		<span>Berita</span>
      	</a>
  	</li>
</ul>