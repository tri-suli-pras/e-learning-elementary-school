@extends("dashboard.management.home")

@push("css")
<link rel="stylesheet" href="{{ asset('web/customs/css/option-hover.css') }}">
@endpush

@section("content")
<div class="content-header">
    <div>
        <a href="{{ route('task') }}"> 
            <div class="fa fa-angle-double-left mr-2"></div>
            {{ trans('label.index_page_of', ['what' => "Tugas"]) }}
        </a>
        &nbsp;<b>||</b>&nbsp;
        <a href="{{ route('task.add') }}"> 
            {{ trans('label.act.add', ['what' => "Tugas"]) }}
            <div class="fa fa-angle-double-right mr-2"></div>
        </a>
    </div>

    <h2 class="title">
    	{!! trans('label.detail', ['for' => "Tugas", 'what' => $task->lessons->nama_matapelajaran]) !!}
    </h2>
	@if (session("update") || session('update_score'))
	    <div class="right-content" id="session">
	    	<span>
	    		<div class="alert alert-success alert-dismissible fade show" role="alert">
	    			<span class="lead">{{ trans('confirmation.success') }}</span>
	    			{{ session('update') ? session('update') : session('update_score') }}
	                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
	                	<span aria-hidden="true">×</span>
	                </button>
	            </div>
	    	</span>
	    </div>
	@endif
</div>
<div class="content-body">
	<div class="row">
		<div class="col-md-12">
			<dl id="dlRow" class="row">
				<dt class="col-4"><h4>{{ trans('label.for.-T') }}</h4></dt>
				<dd class="col-8">{{ $task->title }}</dd>

				<dt class="col-4"><h4>{{ trans('label.for.-C') }}</h4></dt>
				<dd class="col-8">{{ "{$task->class_room} {$task->pararel}" }}</dd>

				<dt class="col-4"><h4>{{ trans('label.for.-D') }}</h4></dt>
				<dd class="col-8">@if (null($task->description) || $task->description == '')
					<b>{{ trans('label.empty') }}</b>
					@else
					{{ $task->description }}
				@endif</dd>
				
				<dt class="col-4"><h4>{{ trans('label.for.--D') }}</h4></dt>
				<dd class="col-8">{{ \Carbon\Carbon::parse($task->deadline)->format('d F Y H:i') }}</dd>

				<dt class="col-4"><h4>{{ trans('label.task_file') }}</h4></dt>
				<dd class="col-8">
					@empty ($task->file_path)
					    <b>{{ trans('label.empty') }}</b>
					@else
						<a href="{{ asset($task->file_path) }}" download="{{ $task->title . '.' .explode('.', $task->file_path)[1] }}">
							{{ $task->file_name }}
						</a>
					@endempty
				</dd>
				
				<dt class="col-4"><h4>{{ trans('label.for.-H') }}</h4></dt>
				<dd class="col-8">{{ \Carbon\Carbon::parse($task->created_at)->diffForHumans() }}</dd>
				
				<dt class="col-4"><h4>{{ trans('label.for.-O') }}</h4></dt>
				<dd class="col-8">
					@if ($task->user_id === auth()->user()->id)
					<a class="badge badge-primary" href="{{ route('task.edit', $task->id) }}">{{ trans('label.act.edit') }}</a>
					<a class="badge badge-danger" href="{{ '#delete'.$task->id }}" data-toggle="modal">{{ trans('label.act.delete') }}</a>
					@endif
					<a class="badge badge-info" href="{{ '#scores'.$task->id }}" data-toggle="modal">{{ trans('label.score') }}</a>
				</dd>
			</dl>
		
			<hr />
		</div>
	</div>
</div>
@endsection

@section("modal")
	<div class="modal fade" id="{{ 'scores'.$task->id }}" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">{{ $task->title }}</h5>
	                <button class="close" type="button" data-dismiss="modal" aria-label="Close" title="Tutup" data-toggle="tooltip">
	                    <img src="{{ asset('web/images/icon-close.png') }}"/>
	                </button>
	            </div>
	            @include('forms.modal.task_detail_student_scores')
	        </div>
	    </div>
	</div>

	<div class="modal fade" id="{{ 'delete'.$task->id }}" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">{{ trans('label.act.delete') }}</h5>
	                <button class="close" type="button" data-dismiss="modal" aria-label="Close" title="Batal" data-toggle="tooltip">
	                    <img src="{{ asset('web/images/icon-close.png') }}"/>
	                </button>
	            </div>
	            <div class="modal-body">
	                <p>{{ trans('confirmation.sure_to_delete', ['this' => "Tugas"]) }}</p>
	            </div>
	            <div class="modal-footer">
	                <form action="{{ route('task.delete', $task->id) }}" method="POST">
	                	{{ csrf_field() }} {{ method_field("DELETE") }}
	                	<button class="btn btn-danger btn-round" type="submit">{{ trans('label.btn.rm') }}</button>
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/SessionMessages.js') }}"></script>
<script>
	$(document).ready(function () {

		$("#task").DataTable({
			"language": {
				"lengthMenu": "Tampil _MENU_",
                "zeroRecords": "Tidak dapat menemukan - maaf",
                "info": "Menampilkan _START_ sampai _END_ dari total _TOTAL_ data",
                "infoEmpty": "Data tidak ditemukan",
                "infoFiltered": "(filter dari _MAX_ total data)",
                "search": "Cari"
			} ,
			"aLengthMenu": [
                [5, 10], 
                [5, 10]
            ]
		})

		$("a.lock").each(function( ) {
            $(this).click(function ( event ) {
                event.preventDefault();
                var isConfirmed = confirm("{{ trans('confirmation.sure_to_lock', ['score' => "Tugas"]) }}");

                if (isConfirmed) {
                    var uri = $(this).attr('href');
                    $.ajax({
                        url: uri,
                        type: "PATCH",
                        data: {'_token': "{{ csrf_token() }}"},
                        success: function ( data ) {
                            alert(data);
                            window.location.reload();
                        },
                        error: function ( data ) {
                            alert(data);
                        }
                    });
                }
            })
        })

        @if ($errors->any())
        	$("#{{ 'scores'.$task->id }}").modal("show");
        @endif

        @if (session('update_score-status'))
        	$("#{{ 'scores'.$task->id }}").modal("show");
        @endif
		
		@foreach ($task->students as $key => $student)
				
			$('a#edit{{ $student->pivot->student_id }}').click(function ( event ) {
				
				event.preventDefault();

				let input = $(this).parent().parent().parent().prev().children();
				let span = $(this).parent().parent().parent().prev().children().next();
				input.show();
				span.hide();
			});
		@endforeach
		
	});
</script>
@endpush

