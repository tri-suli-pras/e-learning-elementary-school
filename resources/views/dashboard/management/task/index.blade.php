@extends("dashboard.management.home")

@push("css")
<link rel="stylesheet" href="{{ asset('web/customs/css/option-hover.css') }}">
@endpush

@section("content")
<div class="content-header">
	<h2 class="title">{{ trans('label.index_page_of', ['what' => "Tugas"]) }}</h2>
    <div class="right-content">
    	<a class="btn btn-primary pull-right btn-add" href="{{ route('task.add') }}">
    		<span>{{ trans('label.act.add', ['what' => "Tugas"]) }}</span>
    	</a>
    </div>
    @component('components.search_periode')@endcomponent
</div>
@if (session("store") || session("delete") || session("update"))
<div id="session" class="alert alert-success alert-dismissible fade show" role="alert">
    <span class="lead">
        {{ trans('confirmation.success') }} {{ session("store") ? session("store") : (session("delete") ? session('delete') : session('update')) }}
    </span>
    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>  
@endif
<div class="content-body">
	<div class="table-responsive-md">
		<table id="task" class="table table-custom">
            <thead>
                <th width="1%">{{ trans('label.for.#') }}</th>
                <th width="20%">{{ trans('label.for.-L') }}</th>
                <th width="25%">{{ trans('label.for.-T') }}</th>
                <th width="10%">{{ trans('label.for.-C') }}</th>
                @role('admin')<th>{{ trans('label.for.by') }}</th>@endrole
                <th width="1%"></th>
            </thead>

            <tbody>
                @empty ($datas->toArray())
                    <tr id="dlRow">
                        <td colspan="7" align="center"><b>{{ trans('label.empty') }}</b></td>
                    </tr>
                @else
                    @foreach ($datas as $key => $task)
                    <tr id="dlRow">
                        <td align="center">{{ $loop->iteration }}</td>
                        <td class="sempak">{{ $task->lessons->nama_matapelajaran }}</td>
                        <td align="center">{{ $task->title }}</td>
                        <td align="center">{{ "{$task->class_room} {$task->pararel}" }}</td>
                        @role('admin')
                        <td align="center">{{ is_null($task->teachers->employees['nama_pegawai']) ? "Admin" : $task->teachers->employees['nama_pegawai'] }}</td>
                        @endrole
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-outline-secondary btn-sm" type="button" data-toggle="dropdown">{{ trans('label.for.-O') }}</button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item dtl" href="{{ route('task.detail', $task->id) }}">{{ trans('label.act.detail') }}</a>
                                    @if ($task->user_id === auth()->user()->id)
                                    <a class="dropdown-item edit" href="{{ route('task.edit', $task->id) }}">
                                        {{ trans('label.act.edit') }}
                                    </a>
                                    <a class="dropdown-item del" href="{{ '#delete'.$task->id }}" data-toggle="modal">
                                        {{ trans('label.act.delete') }}
                                    </a>
                                    @endif
                            </div>
                        </td>
                    </tr>
                    @endforeach
                @endempty
            </tbody>
		</table>
	</div>
</div>
@endsection

@push("javascript")
<script src="{{ asset('web/customs/js/SessionMessages.js') }}"></script>
<script src="{{ asset('web/customs/js/DataTable.js') }}"></script>
<script>
    @if (!empty($datas->toArray()))
        $(document).ready(function () {
            new TableController("#task");
        })
    @endif
</script>
@endpush