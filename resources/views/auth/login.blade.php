<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>MI Baiturahman E-Learning</title>
    <meta name="description" content=""/>
    <link rel="stylesheet" href="{{ asset('web/styles/plugins.css') }}"/>
    <link rel="stylesheet" href="{{ asset('web/styles/main.css') }}"/>
</head>
<body class="login">
    <div id="wrap">
        <main>
            <div class="wrap">
                <div class="section">
                    <div class="container-fluid auth-page">
                        <div class="row">
                            <div class="col-md-10 left-wrap">
                                @yield("background")
                            </div>

                            <div class="col-md-2 right-wrap">
                                <div class="form-wrap">
                                    <div class="box">
                                        <h1 class="logo"><img src="{{ asset('web/images/harnods-logo.svg') }}"/>
                                            E-Learning
                                        </h1>
                                        <p>Silahkan login untuk masuk ke halaman utama</p>
                                        @if (session('notAuthenticated'))
                                            <span id="failure" style="color: red">{{ session("notAuthenticated") }}</span>
                                        @endif
                                        @yield("form")
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mask"></div>
            </div>
        </main>
    </div>
</body>
    <script src="{{ asset('web/plugins/jquery-3.3.1/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('web/plugins/bootstrap-4/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('web/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('web/scripts/main.js') }}"></script>
    <script>
        $(function () {
            if ($("#failure"))
            {
                setTimeout(function () {
                   $("#failure").css('display', "none");     
                }, 5000);
            }
        });
    </script>
</html>