<form action="{{! isset($task) ? route('task.save') : route('task.update', $task->id)}}" method="POST" class="mb-5" enctype="multipart/form-data" autocomplete="off">
	{{ csrf_field() }} @if (isset($task))
		{{ method_field("PATCH") }}
	@endif

	<div class="form-group" hidden>
		<input name="user_id" type="number" value="{{ auth()->user()->id }}">
	</div>

	<div class="form-group">
		<label for="title">{{ trans('label.for.-T') }}</label>
		<input id="title" 
               name="title"
               type="text"
               class="form-control {{ $errors->has("title") ? 'is-invalid' : '' }}"
               placeholder="{{ trans('label.placeholder.max_75') }}"
               value="{{ isset($task) ? $task->title : (old('title') ? old('title') : "") }}"
        /> 
        @if ($errors->has('title')) <div class="invalid-feedback">{{ $errors->first('title') }}</div>@endif
	</div>

	<div class="row">
        <div class="form-group col-md-4 col-sm-6">
            <label for="class_room">{{ trans('label.for.-C') }}</label>
            <select id="class_room"
                    name="class_room"
                    class="select-control {{ !$errors->first('class_room') ?: 'is-invalid' }}"
                    title="Pilih...">
                @foreach ($classes as $classroom)
                    @if ( isset($task) && (is_equal(is_equal($task->class_room, $classroom->nama_kelas), is_equal($task->pararel, $classroom->palarel))) )
                        <option value="{{ "$classroom->nama_kelas $classroom->palarel" }}" selected>{{ "$classroom->nama_kelas $classroom->palarel" }}</option>
                    @else
                        <option value="{{ "$classroom->nama_kelas $classroom->palarel" }}" {{ is_equal(old('class_room'), "$classroom->nama_kelas $classroom->palarel") ? "selected" : '' }}>{{ "$classroom->nama_kelas $classroom->palarel" }}</option>
                    @endif
                @endforeach
            </select>
            <div class="invalid-feedback" style="display: inline;">{{ $errors->first('class_room') }}</div> 
        </div>

        <div class="form-group col-md-8 col-sm-6">
            <label for="lesson_id">{{ trans('label.for.-L') }}</label>
            <select id="lesson_id" 
                    name="lesson_id"
                    class="select-control {{ !$errors->first('lesson_id') ?: 'is-invalid' }}"
                    title="Pilih..."
                    data-live-search="true" style="{{ $errors->has('lesson_id') ? 'border: 1px solid red;' : '' }}">
                @foreach ($lessons as $lesson)
                    @if (isset($task) && (is_equal($task->lesson_id, $lesson->id)) )
                        <option value="{{ $lesson->id }}" data-subtext="{{ $lesson->curriculums->nama_kurikulum }}" selected>{{ $lesson->nama_matapelajaran }}</option>
                    @else
                        <option value="{{ $lesson->id }}" data-subtext="{{ $lesson->curriculums->nama_kurikulum }}" {{ is_equal((int) old('lesson_id'), $lesson->id) ? "selected" : '' }}>{{ $lesson->nama_matapelajaran }}</option>
                    @endif
                @endforeach
            </select>
            <div class="invalid-feedback" style="display: inline;">{{ $errors->first('lesson_id') }}</div> 
        </div>
    </div>

    <div class="form-group">
		<label for="description">{{ trans('label.for.-D') }}</label>
        <textarea id="description" name="description" class="form-control {{ !$errors->first('description') ?: 'is-invalid' }}" rows="5" title="Harap Diisi jika tidak menyertakan file" data-toggle="tooltip">{{ isset($task) ? $task->description : (old('description') ? old('description') : "") }}</textarea>
        <div class="invalid-feedback" style="display: inline;">{{ $errors->first('description') }}</div> 
	</div>

    <div class="form-group">
        <label for="datetime">{{ trans('label.for.--D') }}</label>
        <div class="date-control">
            <div class="input-group date" id="datetimepicker" data-target-input="nearest">
                <input id="datetime" 
                       name="datetime"
                       type="text" 
                       class="form-control datetimepicker-input {{ !$errors->first('datetime') ?: 'is-invalid' }}" 
                       data-target="#datetimepicker" 
                       title="Tanggal dan Jam" 
                       data-toggle="tooltip" 
                       placeholder="bln-tgl-th j:m "
                       value="{{ isset($task) ? datetimepicker($task->deadline) : (old('datetime') ? datetimepicker(old('datetime')) : '') }}" 
                />
                <div id="picker" class="input-group-append" data-target="#datetimepicker" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>
        <div class="invalid-feedback datetime" style="display: inline;">{{ $errors->first('datetime') }}</div> 
    </div>

	<div class="form-group">
    <div class="custom-file">
      <input id="file_path" 
           name="file_path" 
           type="file" 
           class="custom-file-input {{ !$errors->first('file_path') ?: 'is-invalid' }}" 
           id="file_path"  
           value="{{ old('file_path') ? old('file_path') : '' }}" />
          <div class="invalid-feedback" style="display: inline;">{{ $errors->first('file_path') }}</div> 
        <label class="custom-file-label" for="file_path">{{ isset($task) ? $task->file_name : trans('label.file.!q') }}</label>
    </div>
  </div>
  	<div class="form-action">
      <a class="btn btn-outline-secondary btn-round" href="{{ route('task') }}" title="{{ trans('label.back_to', ['what' => "Tugas"]) }}" data-toggle="tooltip">{{ trans('label.btn.cn') }}</a>
    	<button class="btn btn-primary btn-round" type="submit">{{ !isset($task) ? trans('label.btn.sv') : trans('label.btn.ud') }}</button>
  	</div>
</form>